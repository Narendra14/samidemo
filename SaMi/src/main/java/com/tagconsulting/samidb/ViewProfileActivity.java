package com.tagconsulting.samidb;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.tagconsulting.samidb.JSON.JSONParser;
import com.tagconsulting.samidb.adapter.ProfileListAdapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

@SuppressLint("ShowToast")
public class ViewProfileActivity extends ListActivity {

	private ProfileManagerApplication app;
	private ProfileListAdapter adapter;
	private AlertDialog removeUploadDialog;
	private CharSequence toastText;
	private boolean loggedin;
	private String authorization;
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		
		setContentView(R.layout.view_profiles);
		app = (ProfileManagerApplication) getApplication();
		
		adapter = new ProfileListAdapter(app.getCurrentProfile(), this);
		
		setListAdapter(adapter);
		}
	
	

	private void actionAdd() {
		Intent intent = new Intent(ViewProfileActivity.this, AddProfileActivity.class);
		intent.putExtra("position", -1);		
		startActivity(intent);
	}
	
	public void editActivity(int position){
		Intent intent = new Intent(ViewProfileActivity.this, AddProfileActivity.class);
		intent.putExtra("position", position);		
		startActivity(intent);
	}
	
	private void login() {
		if (isNetworkAvailable()){
		Intent intent = new Intent(ViewProfileActivity.this, LoginActivity.class);
		startActivity(intent);
		}else{
			Toast.makeText(this, "No internet connectivity", Toast.LENGTH_SHORT).show();
		}
	}
	
	private void upload(){
		//app.getJSONFromDB();
	if(isNetworkAvailable()){
		new UploadProfile().execute();
	}else{
		Toast.makeText(this, "No internet connectivity", Toast.LENGTH_SHORT).show();
	}
	}

	private void removeUploadedProfile() {
		app.deleteProfile(adapter.removeUploadedProfile());
	}

	private boolean isNetworkAvailable() {
	    ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	private Menu menu;
	// Initiating Menu XML file (menu.xml)
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);

		// Get the SearchView and set the searchable configuration
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) menu.findItem(R.id.action_search)
				.getActionView();
		// Assumes current activity is the searchable activity
		searchView.setSearchableInfo(searchManager
				.getSearchableInfo(getComponentName()));
		searchView.setIconifiedByDefault(false); // Do not iconify the widget;
													// expand it by default

		if(loggedin){
			(menu.findItem(R.id.action_login)).setTitle(R.string.logout);
		}else{
			(menu.findItem(R.id.action_login)).setTitle(R.string.login);
		}
		this.menu = menu;
		return true;

	}

	/**
	 * Event Handling for Individual menu item selected Identify single menu
	 * item by it's id
	 * */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_search:
            onSearchRequested();
            return true;
		case R.id.action_add:
			actionAdd();
			break;

		case R.id.action_remove_uploaded:
			removeUploadDialog = new AlertDialog.Builder(this)
			.setTitle(R.string.delete_title)
			.setMessage(R.string.delete_message)
			.setPositiveButton(R.string.delete_positive,
					new AlertDialog.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog,
								int which) {
							removeUploadedProfile();

						}
					})
			.setNeutralButton(R.string.delete_negative,
					new AlertDialog.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog,
								int which) {
							removeUploadDialog.cancel();

						}
					}).create();
			removeUploadDialog.show();
			
			break;

		case R.id.action_login:
			if (!loggedin) {
				login();
			} else {
				app.clearAuthorization();
				(menu.findItem(R.id.action_login)).setTitle(R.string.login);
				loggedin = false;
			}
				
			break;
			
			
		case R.id.action_upload:
			if (loggedin) {
				upload();
			} else {
				Toast.makeText(this, "Please login first", Toast.LENGTH_SHORT).show();
			}
			break;

		default:
			return super.onOptionsItemSelected(item);
		}

		//Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();

		return true;
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		
		authorization = ((ProfileManagerApplication) getApplication()).getAuthorizationCode();
		if(!TextUtils.isEmpty(authorization))Log.d("logged",authorization);
		loggedin =((ProfileManagerApplication) getApplication()).isLoggedin();
		if(menu!=null){
		if(loggedin){
			(menu.findItem(R.id.action_login)).setTitle(R.string.logout);
		}else{
			(menu.findItem(R.id.action_login)).setTitle(R.string.login);
		}
		}
		adapter.forcereload();
		

	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		//Toast.makeText(this, position+" position", Toast.LENGTH_SHORT).show();
		editActivity(position);
		//adapter.toggleProfileCompleteAtPosition(position);
		//SaMProfile p = (SaMProfile) adapter.getItem(position);
		//app.saveProfile(p);
	}

	// private void setUpViews(){
	// mSearchText = (Button)findViewById(R.id.search_result);
	/*
	 * addButton.setOnClickListener(new View.OnClickListener() {
	 * 
	 * @Override public void onClick(View arg0) {
	 * 
	 * 
	 * } });
	 */

	// }
	
	
	
	/**
	 * Background Async Task to Create new product
	 * */
	private class UploadProfile extends AsyncTask<String, String, String> {
		
		
		// Progress Dialog
		private ProgressDialog pDialog;
		private String message;
		
				JSONParser jsonParser = new JSONParser();

				
				

				// JSON Node names
				private static final String TAG_SUCCESS = "status";
				private static final String TAG_DATA = "data";

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@SuppressWarnings({ "deprecation" })
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			//setProgressBarIndeterminateVisibility(true);
			pDialog = new ProgressDialog(ViewProfileActivity.this);
			pDialog.setTitle("Uploading ...");
			pDialog.setCanceledOnTouchOutside(false);
			pDialog.setMessage("Please wait...");
			
			pDialog.setOnCancelListener(new DialogInterface.OnCancelListener(
					) {
				
				@Override
				public void onCancel(DialogInterface dialog) {
					cancel(true);
					
				}
			});
			        

			pDialog.setButton("Cancel", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					
					pDialog.cancel();
					Log.d("Progress","Cancelled");
					
				}
			});
			
			pDialog.setCancelable(true);
			pDialog.show();
			
		}

		/**
		 * Creating product
		 * */
		@SuppressWarnings("static-access")
		protected String doInBackground(String... args) {
			
			// getting JSON Object
			// Note that create product url accepts POST method
			JSONArray jsonArray = app.getJSONFromDB();
			int size = jsonArray.length();
			pDialog.setProgressStyle(pDialog.STYLE_HORIZONTAL);
			pDialog.setProgress(0);
	        pDialog.setMax(size);
	        if(size==0)
	        	toastText="No data to upload";
	        else{
	        	
			for (int index=0;index<size;index++){
				
			JSONObject json;
			
			try {
				JSONObject obj=  jsonArray.getJSONObject(index);
				message = "uploading "+ obj.getString("fullName");
				
				json = jsonParser.makeHttpRequest("POST",authorization, obj);
				// check log cat fro response
				//Log.d("Create Response", json.toString());

				// check for success tag
				if(json == null){
					toastText = "Server Unavailable";
					return null;
				}
					int success = json.getInt(TAG_SUCCESS);

					if (success == 200) {
						publishProgress(message);
						// successfully created product
						/*Intent i = new Intent(getApplicationContext(), ViewProfileActivity.class);
						startActivity(i);*/
						Log.d("Success",(index + 1)+" of "+ size + " uploaded");
						
						app.updateProfile(obj.getLong("id"));
						//pDialog.setTitle(title);
						//pDialog.set
						toastText = size+"/"+size+" profile uploaded.";
						pDialog.setProgress(index+1);
						// closing this screen
						//finish();
					} else {
						toastText = "Upload failed: "+ json.getString(TAG_DATA) + " on " + obj.getString("fullName");
						break;
					}
					
				
			} catch (JSONException e) {
				
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (isCancelled()) break;
			
			
			}
			
			
	        }
			//adapter.forcereload();
			
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once done
			//setProgressBarIndeterminateVisibility(false);
			pDialog.dismiss();
			if(!TextUtils.isEmpty(toastText)){
				Toast.makeText(ViewProfileActivity.this, toastText, Toast.LENGTH_LONG).show();
				toastText=null;
				}
			adapter.forcereload();
		}
		
		public void onProgressUpdate(String... values) {
			super.onProgressUpdate(values);
	        pDialog.setMessage(values[0]);	         
	        
	    }

		 
		 
		
	}
	
	
	

}
