package com.tagconsulting.samidb.adapter;

import java.util.ArrayList;

import com.tagconsulting.samidb.R;
import com.tagconsulting.samidb.profile.SaMProfile;
import com.tagconsulting.samidb.views.ProfileListItem;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class ProfileListAdapter extends BaseAdapter {

	private ArrayList<SaMProfile> profiles;
	private Context context;
	
	public ProfileListAdapter(ArrayList<SaMProfile> profiles, Context context) {
		super();
		this.profiles = profiles;
		this.context = context;
	}

	@Override
	public int getCount() {
		return profiles.size();
	}

	@Override
	public Object getItem(int position) {
		return (null == profiles)? null : profiles.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ProfileListItem pli;
		if (null == convertView){
			pli = (ProfileListItem) View.inflate(context,R.layout.profile_list_item, null);
		} else {
			pli = (ProfileListItem) convertView;
		}
		pli.setProfile(profiles.get(position));
		return pli;
	}

	public void forcereload() {
		notifyDataSetChanged();
		
	}

	/*public void toggleProfileCompleteAtPosition(int position) {
		SaMProfile p = profiles.get(position);
		p.toggleComplete();
		notifyDataSetChanged();
		
	}*/
	
	public Long[] removeUploadedProfile() {
		ArrayList<SaMProfile> uploadedProfile = new ArrayList<SaMProfile>();
		ArrayList<Long> completedIds = new ArrayList<Long>();
		for (SaMProfile profile: profiles){
			if(profile.isUploaded()){
				uploadedProfile.add(profile);
				completedIds.add(profile.getId());
			}
		}
		profiles.removeAll(uploadedProfile);
		notifyDataSetChanged();
		return completedIds.toArray(new Long[]{});
	}

	public Long[] removeSelectedProfile(long id){
		Long ids = null;
		ArrayList<SaMProfile> uploadedProfile = new ArrayList<SaMProfile>();
		for (SaMProfile profile: profiles){
			if(profile.getId() == id){
				uploadedProfile.add(profile);
				ids = id;
			}
		}
		profiles.remove(uploadedProfile);
		notifyDataSetChanged();
		return new Long[]{ids};
	}
	
	public Long[] matchedProfile(String s) {
		
		ArrayList<Long> matchedIds= new ArrayList<Long>();
		for (SaMProfile profile: profiles){
			if(profile.getProfile().toString().contains(s)){
				matchedIds.add(profile.getId());
			}
		}
		return matchedIds.toArray(new Long[]{});
	}

}
