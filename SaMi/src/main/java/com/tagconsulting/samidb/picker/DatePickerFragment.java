package com.tagconsulting.samidb.picker;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.DatePicker;
import android.widget.EditText;

@SuppressLint("ValidFragment")
public class DatePickerFragment extends DialogFragment implements
		DatePickerDialog.OnDateSetListener {

	EditText date;

	@SuppressLint("ValidFragment")
	public DatePickerFragment(EditText editText) {
		this.date = editText;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the current date as the default values for the picker
		int year;
		int month;
		int day;
		if (TextUtils.isEmpty(date.getText())) {
			final Calendar c = Calendar.getInstance();
			year = c.get(Calendar.YEAR);
			month = c.get(Calendar.MONTH);
			day = c.get(Calendar.DAY_OF_MONTH);
		} else {
			String[] d = date.getText().toString().split("/");
			month = Integer.parseInt(d[0]) - 1;
			day = Integer.parseInt(d[1]);
			year = Integer.parseInt(d[2]);
		}

		// Create a new instance of DatePickerDialog and return it
		return new DatePickerDialog(getActivity(), this, year, month, day);
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		date.setError(null);
		date.setText(new StringBuilder()
        // Month is 0 based, just add 1
        .append(monthOfYear + 1).append("/").append(dayOfMonth).append("/")
        .append(year));

	}
}
