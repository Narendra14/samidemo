package com.tagconsulting.samidb;

import static com.tagconsulting.samidb.db.DBHelper.*;

import java.util.ArrayList;

import com.tagconsulting.samidb.adapter.ProfileListAdapter;
import com.tagconsulting.samidb.db.DBHelper;
import com.tagconsulting.samidb.profile.SaMProfile;
import com.tagconsulting.samidb.provider.SuggestionProvider;

import android.annotation.TargetApi;
import android.app.ListActivity;
import android.app.SearchManager;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Build;
import android.provider.SearchRecentSuggestions;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class SearchableActivity extends ListActivity {
 
    private SQLiteDatabase database;
	private ArrayList<SaMProfile> currentProfiles;
	private ProfileListAdapter adapter;
	private ProfileManagerApplication app;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.view_profiles);
        
        // implement Up Navigation with caret in front of App icon in the Action Bar
        getActionBar().setDisplayHomeAsUpEnabled(true);
 
        Intent intent = getIntent();
        checkIntent(intent);
    }

	// Initiating Menu XML file (menu.xml)
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.search_menu, menu);
		
			return true;

		}
	
	private void clearSearchHistory() {
		SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
				SuggestionProvider.AUTHORITY, SuggestionProvider.MODE);
		suggestions.clearHistory();
	}
 
    @Override
    protected void onNewIntent(Intent newIntent) {
        // update the activity launch intent
        setIntent(newIntent);
        // handle it
        checkIntent(newIntent);
    }
 
    private void checkIntent(Intent intent) {
        String query = "";
        String intentAction = intent.getAction();

        if (Intent.ACTION_SEARCH.equals(intentAction)) {
            query = intent.getStringExtra(SearchManager.QUERY).trim();
            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(
					this, SuggestionProvider.AUTHORITY, SuggestionProvider.MODE);
			suggestions.saveRecentQuery(query, null);
        } else if (Intent.ACTION_VIEW.equals(intentAction)) {
 
            Uri details = intent.getData();
            Intent detailsIntent = new Intent(Intent.ACTION_VIEW, details);
            startActivity(detailsIntent);
 
        }
        fillList(query);
    }
 
    private void fillList(String query) {
    	currentProfiles = new ArrayList<SaMProfile>();
        String wildcardQuery = "%" + query + "%";
        DBHelper helper = new DBHelper(this);
		database = helper.getWritableDatabase();
		
        Cursor tasksCursor = database.query(PROFILE_TABLE, new String[] {
				PROFILE_ID, INFORMATION_SOURCE,
				REGISTRATION_DATE,DISTRICT,
				GENDER,FIRST_NAME,MIDDLE_NAME,LAST_NAME,
				AGE,ETHNICITY,CASTE, DISCRIMINATION,PHONE,PHONE_FAMILY, 
				PERMANENT_DETAILS,PERMANET_WARD, PERMANENT_VDC, PERMANENT_DISTRICT, 
				TEMPORARY_SAME_AS_PERMAENT,TEMPORARY_DETAILS,TEMPORARY_WARD, TEMPORARY_VDC, TEMPORARY_DISTRICT,
				EDUCATIONAL_STATUS,MARITAL_STATUS,FAMILY_MEMBERA, FAMILY_MEMBERB,FAMILY_MEMBERC, FAMILY_MEMBERD,
				PRESENT_OCCUPATION,PROFILE_UPLOADED,PICTURE,PROFILE_NAME,REGISTRATION_NUMBER,HOW_YOU_KNOW_IC,FREQUENCY_OF_VISIT,
				REASON_FOR_VISITING,GPS_LONGITUDE,GPS_LATITUDE}, PROFILE_NAME + " LIKE ?", 
                new String[] { wildcardQuery },
				null, null, String.format("%s,%s", PROFILE_UPLOADED, PROFILE_NAME));
         
        tasksCursor.moveToFirst();
		SaMProfile p;
		if (!tasksCursor.isAfterLast()) {
			do {
				int id = tasksCursor.getInt(0);
				
				String informationSource = tasksCursor.getString(1);
				String registrationDate = tasksCursor.getString(2);
				String district = tasksCursor.getString(3);
				String gender = tasksCursor.getString(4);
				String fname = tasksCursor.getString(5);
				String mname = tasksCursor.getString(6);
				String lname = tasksCursor.getString(7);
				String age = tasksCursor.getString(8);
				String ethnicity = tasksCursor.getString(9);
				String caste = tasksCursor.getString(10);
				boolean discrimination = Boolean.parseBoolean(tasksCursor.getString(11));

				String phone = tasksCursor.getString(12);
				String phoneFamily = tasksCursor.getString(13);
				
				String permanentDetails = tasksCursor.getString(14);
				String permanentWard = tasksCursor.getString(15);
				String permanentVDC = tasksCursor.getString(16);
				String permanentDistrict = tasksCursor.getString(17);
				boolean temporarySameAsPermanent = Boolean.parseBoolean(tasksCursor.getString(18));
				String temporaryDetails = tasksCursor.getString(19);
				String temporaryWard = tasksCursor.getString(20);
				String temporaryVDC = tasksCursor.getString(21);
				String temporaryDistrict = tasksCursor.getString(22);
				
				String educationalStatus = tasksCursor.getString(23);
				String maritalStatus = tasksCursor.getString(24);
				String familyMembersA = tasksCursor.getString(25);
				String familyMembersB = tasksCursor.getString(26);
				String familyMembersC = tasksCursor.getString(27);
				String familyMembersD = tasksCursor.getString(28);
				String presentOccupation = tasksCursor.getString(29);
				boolean isUploaded = Boolean.parseBoolean(tasksCursor.getString(30));
				byte[] picture = tasksCursor.getBlob(31);
				String fullName = tasksCursor.getString(32);
				String registrationNumber =tasksCursor.getString(33);
				String howYouKnowIC=tasksCursor.getString(34);
				String frequencyOfVisit=tasksCursor.getString(35);
				String reasonForVisiting=tasksCursor.getString(36);
				Double longitude=tasksCursor.getDouble(37);
				Double latitude=tasksCursor.getDouble(38);
				
				p = new SaMProfile();
				p.setId(id);
				p.setPicture(picture);
				p.setInformationSource(informationSource);
				p.setRegistrationDate(registrationDate);
				p.setDistrict(district);
				p.setGender(gender);
				p.setFname(fname);
				p.setMname(mname);
				p.setLname(lname);
				p.setAge(age);
				p.setCasteEthnicity(ethnicity);
				p.setCasteCaste(caste);
				p.setDiscriminated(discrimination);
				p.setPhone(phone);
				p.setPhoneFamily(phoneFamily);
				
				p.setPermanentDetails(permanentDetails);
				p.setPermanentWard(permanentWard);
				p.setPermanentVDC(permanentVDC);
				p.setPermanentDistrict(permanentDistrict);
				p.setTemporarySameAsPermanent(temporarySameAsPermanent);
				p.setTemporaryDetails(temporaryDetails);
				p.setTemporaryWard(temporaryWard);
				p.setTemporaryVDC(temporaryVDC);
				p.setTemporaryDistrict(temporaryDistrict);
				p.setEducationalStatus(educationalStatus);
				p.setMaritalStatus(maritalStatus);
				p.setFamilyMembersA(familyMembersA);
				p.setFamilyMembersB(familyMembersB);
				p.setFamilyMembersC(familyMembersC);
				p.setFamilyMembersD(familyMembersD);
				p.setPresentOccupation(presentOccupation);
				p.setUploaded(isUploaded);
				p.setProfileName(fullName);
				p.setRegistrationNumber(registrationNumber);
				p.setHowYouKnowIC(howYouKnowIC);
				p.setFrequencyOfVisit(frequencyOfVisit);
				p.setReasonForVisiting(reasonForVisiting);
				p.setLongitude(longitude);
				p.setLatitude(latitude);
				currentProfiles.add(p);
			} while (tasksCursor.moveToNext());
		}

		tasksCursor.close();
        adapter = new ProfileListAdapter(currentProfiles, this);
        setListAdapter(adapter);
    }
 
    @Override
    protected void onListItemClick(ListView l, View view, int position, long id) {
 
    	Intent intent = new Intent(SearchableActivity.this, AddProfileActivity.class);
    	app = (ProfileManagerApplication) getApplication();
    	int pos=0;
    	for(SaMProfile sp:app.getCurrentProfile()){
    		if(sp.getId()==currentProfiles.get(position).getId()){
    			intent.putExtra("position", pos);		
    			startActivity(intent);
    			break;
    		}
    		pos++;
    	}
    	//adapter = new ProfileListAdapter(app.getCurrentProfile(), this);
		
 
    }
 
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
 
    	switch (item.getItemId()) {
		
		case R.id.clear_history:
			clearSearchHistory();
			
			break;
			
		case android.R.id.home:
 
            // This is called when the Home (Up) button is pressed
            // in the Action Bar.
            Intent parentActivityIntent = new Intent(this, ViewProfileActivity.class);
            parentActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(parentActivityIntent);
            finish();
            break;
            

		default:
			return super.onOptionsItemSelected(item);
		}

		//Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();

		return true;
    	
       
        
 
    }
 
}
