package com.tagconsulting.samidb;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.Toast;

import com.tagconsulting.samidb.gps.GPSTracker;
import com.tagconsulting.samidb.picker.DatePickerFragment;
import com.tagconsulting.samidb.profile.SaMProfile;
import com.tagconsulting.samidb.utilities.ImageUtils;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class AddProfileActivity extends ProfileManagerActivity {

	// UI references

	private Button addButton;
	private Button cancelButton;
	
	private ImageView picture;
	private Spinner informationSource;
	// private EditText profileNumber;
	private EditText registrationDate;
	private Spinner district;
	private RadioGroup gender;
	private EditText fname;
	private EditText mname;
	private EditText lname;
	private Spinner age;
	private Spinner casteEthnicity;
	private Spinner casteCaste;
	private RadioGroup discrimination;
	private EditText phone;
	private EditText phoneFamily;
	private Spinner permanentDistrict;
	private Spinner permanentVDC;
	private EditText permanentWard;
	private EditText permanentDetails;
	private CheckBox temporarySameAsPermanent;
	private Spinner temporaryDistrict;
	private Spinner temporaryVDC;
	private EditText temporaryWard;
	private EditText temporaryDetails;
	private Spinner educationalStatus;
	private Spinner maritalStatus;
	private Spinner familyMembersA;
	private Spinner familyMembersB;
	private Spinner familyMembersC;
	private Spinner familyMembersD;
	private Spinner presentOccupation;
	private EditText registrationNumber;
	private Spinner howYouKnowIC;
	private Spinner frequencyOfVisit;
	private Spinner reasonForVisiting;
	
	private double latitude;
    private double longitude;
    
    private long id;
	private boolean uploaded;
	
	// GPSTracker class
    GPSTracker gps;

	protected boolean changesPending;
	private AlertDialog unsavedChangesDialog;
	private int position;
	private SaMProfile profile;
	Resources res;
	private int casteSelection;
	private int permanentVDCSelection;
	private int temporaryVDCSelection;
	
	//keep track of camera capture intent
		final int CAMERA_CAPTURE = 1;
		//keep track of cropping intent
		final int PIC_CROP = 2;
		final int PIC_GALLERY = 3;
		//captured picture uri
		private Uri picUri;
		private Uri finalPic;
		
		public static final int MEDIA_TYPE_IMAGE = 1;
		
		// directory name to store captured images and videos
	    private static final String IMAGE_DIRECTORY_NAME = "SaMiDB";


	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_profile);
		
		res = getResources();
		position = getIntent().getExtras().getInt("position");
		// Make sure we're running on Honeycomb or higher to use ActionBar APIs
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			// Show the Up button in the action bar.
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
		setUpViews(position);
		if (position != -1) {
			loadAll();
			if(uploaded)
				addButton.setText(res.getString(R.string.back));
		}
		//todo
		//changesPending = false;
		

	}

	protected void delete() {
		unsavedChangesDialog = new AlertDialog.Builder(this)
				.setTitle(R.string.delete_title)
				.setMessage(R.string.delete_message)
				.setPositiveButton(R.string.delete_positive,
						new AlertDialog.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								deleteProfile();

							}
						})
				.setNeutralButton(R.string.delete_negative,
						new AlertDialog.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								unsavedChangesDialog.cancel();

							}
						}).create();
		unsavedChangesDialog.show();

	}

	protected void editProfile() {
		// validation
				if (!validate()) {
					return;
				}
		getProfileManagerApplication().editProfile(profiler());
		//getProfileManagerApplication().deleteSelectedProfile(profile);
		finish();
	}

	protected void deleteProfile() {
		getProfileManagerApplication().deleteSelectedProfile(profile);
		finish();
	}

	protected void loadAll() {
		
		
		profile = getProfileManagerApplication().getProfile(position);
		if(null!=profile.getPicture()){
		setImageFromBytes(picture,profile.getPicture());
		picture.setVisibility(View.VISIBLE);
		}
		id = profile.getId();
		setSpinnerFromString(informationSource,
				profile.getInformationSource());
		// profileNumber = (EditText) findViewById(R.id.profileNumber);
		registrationNumber.setText(profile.getRegistrationNumber());
		registrationDate.setText(profile.getRegistrationDate());
		setSpinnerFromString(district, profile.getDistrict());
		setRadioFromString(gender, profile.getGender());
		fname.setText(profile.getFname());
		mname.setText(profile.getMname());
		lname.setText(profile.getLname());
		setSpinnerFromString(age, profile.getAge());
		setSpinnerFromString(casteEthnicity,
				profile.getCasteEthnicity());
			
		(casteCaste).setAdapter(initCasteArrayAdapter().get(getSpinnerPosition(casteEthnicity,profile.getCasteEthnicity())));
		setSpinnerFromString(casteCaste,profile.getCasteCaste());

		//todo like a hard code
		casteSelection = getSpinnerPosition(casteCaste,profile.getCasteCaste());
		
		setRadioFromBoolean(discrimination,profile.isDiscriminated());
		(phone).setText(profile.getPhone());
		(phoneFamily).setText(profile.getPhoneFamily());
		setSpinnerFromString(permanentDistrict,profile.getPermanentDistrict());
		(permanentVDC).setAdapter(initVDCArrayAdapter().get(getSpinnerPosition(permanentDistrict,profile.getPermanentDistrict())));
		setSpinnerFromString(permanentVDC,profile.getPermanentVDC());
		
		//todo like a hard code
		permanentVDCSelection = getSpinnerPosition(permanentVDC,profile.getPermanentVDC());
				
		
		(permanentWard).setText(profile.getPermanentWard());
		(permanentDetails).setText(profile.getPermanentDetails());
		(temporarySameAsPermanent).setChecked(profile.isTemporarySameAsPermanent());
		if(profile.isTemporarySameAsPermanent()){
			temporaryDistrict.setClickable(false);
			temporaryVDC.setClickable(false);
			temporaryWard.setEnabled(false);
			temporaryDetails.setEnabled(false);		
			
		}
		setSpinnerFromString(temporaryDistrict,profile.getTemporaryDistrict());
		(temporaryVDC).setAdapter(initVDCArrayAdapter().get(getSpinnerPosition(temporaryDistrict,profile.getTemporaryDistrict())));
		setSpinnerFromString(temporaryVDC,profile.getTemporaryVDC());
		
		//todo like a hard code
		temporaryVDCSelection = getSpinnerPosition(temporaryVDC,profile.getTemporaryVDC());
				
		
		
		(temporaryWard).setText(profile.getTemporaryWard());
		(temporaryDetails).setText(profile.getTemporaryDetails());
		setSpinnerFromString(educationalStatus,profile.getEducationalStatus());
		setSpinnerFromString(maritalStatus,profile.getMaritalStatus());
		setSpinnerFromString(familyMembersA,profile.getFamilyMembersA());
		setSpinnerFromString(familyMembersB,profile.getFamilyMembersB());
		setSpinnerFromString(familyMembersC,profile.getFamilyMembersC());
		setSpinnerFromString(familyMembersD,profile.getFamilyMembersD());
		setSpinnerFromString(presentOccupation,profile.getPresentOccupation());
		setSpinnerFromString(howYouKnowIC,
				profile.getHowYouKnowIC());
		setSpinnerFromString(frequencyOfVisit,
				profile.getFrequencyOfVisit());
		setSpinnerFromString(reasonForVisiting,
				profile.getReasonForVisiting());
		
		uploaded = profile.isUploaded();
	}

	private void setImageFromBytes(ImageView iv, byte[] bt) {
		if( null != bt){
		iv.setImageBitmap(ImageUtils.getImage(bt));
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private int getSpinnerPosition(Spinner sp, String s) {
		int spinnerPosition = ((ArrayAdapter) sp.getAdapter()).getPosition(s);
		return spinnerPosition;
	}

	private void setRadioFromBoolean(RadioGroup rg,
			boolean b) {
		if(b){
			rg.check(rg.getChildAt(0).getId());
		}else{
			rg.check(rg.getChildAt(1).getId());
		}
		
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	protected void cancel() {
		if (changesPending) {
			unsavedChangesDialog = new AlertDialog.Builder(this)
					.setTitle(R.string.unsaved_changes_title)
					.setMessage(R.string.unsaved_changes_message)
					.setPositiveButton(R.string.add_profile,
							new AlertDialog.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									addProfile();

								}
							})
					.setNeutralButton(R.string.cancel,
							new AlertDialog.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									unsavedChangesDialog.cancel();

								}
							})
					.setNegativeButton(R.string.discard,
							new AlertDialog.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									finish();

								}
							}).create();
			unsavedChangesDialog.show();

		} else
			finish();

	}

	private SaMProfile profiler() {
		SaMProfile p = new SaMProfile();
		p.setId(id);
		if(picture.getVisibility()!=View.GONE){
			p.setPicture(ImageUtils.getBytes(((BitmapDrawable)picture.getDrawable()).getBitmap()));
		}
		p.setInformationSource(informationSource.getSelectedItem()
				.toString());
		// p.setProfileNumber(profileNumber.getText().toString());
		p.setRegistrationNumber(registrationNumber.getText().toString());
		p.setRegistrationDate(registrationDate.getText().toString());
		p.setDistrict(district.getSelectedItem().toString());
		p.setGender(((RadioButton) findViewById(gender
				.getCheckedRadioButtonId())).getText().toString());
		p.setFname(fname.getText().toString());
		p.setMname(mname.getText().toString());
		p.setLname(lname.getText().toString());
		p.setAge(age.getSelectedItem().toString());
		p.setCasteEthnicity(casteEthnicity.getSelectedItem().toString());
		p.setCasteCaste(casteCaste.getSelectedItem().toString());
		p.setDiscriminated((((RadioButton) findViewById(discrimination
				.getCheckedRadioButtonId())).getText().toString()
				.equalsIgnoreCase("discriminated")) ? true : false);
		p.setPhone(phone.getText().toString());
		p.setPhoneFamily(phoneFamily.getText().toString());
		p.setPermanentDistrict(permanentDistrict.getSelectedItem().toString());
		
		//todo
		//String selectedVal = getResources().getStringArray(R.array.district)[permanentVDC.getSelectedItemPosition()];
		//Object s = permanentVDC.getSelectedItem();
		
		p.setPermanentVDC(permanentVDC.getSelectedItem().toString());
		p.setPermanentWard(permanentWard.getText().toString());
		p.setPermanentDetails(permanentDetails.getText().toString());
		p.setTemporarySameAsPermanent(temporarySameAsPermanent.isChecked());
		if (temporarySameAsPermanent.isChecked()) {
			p.setTemporaryDistrict(permanentDistrict.getSelectedItem()
					.toString());
			p.setTemporaryVDC(permanentVDC.getSelectedItem().toString());
			p.setTemporaryWard(permanentWard.getText().toString());
			p.setTemporaryDetails(permanentDetails.getText().toString());
		} else {
			p.setTemporaryDistrict(temporaryDistrict.getSelectedItem()
					.toString());
			p.setTemporaryVDC(temporaryVDC.getSelectedItem().toString());
			p.setTemporaryWard(temporaryWard.getText().toString());
			p.setTemporaryDetails(temporaryDetails.getText().toString());
		}
		p.setEducationalStatus(educationalStatus.getSelectedItem().toString());
		p.setMaritalStatus(maritalStatus.getSelectedItem().toString());
		p.setFamilyMembersA(familyMembersA.getSelectedItem().toString());
		p.setFamilyMembersB(familyMembersB.getSelectedItem().toString());
		p.setFamilyMembersC(familyMembersC.getSelectedItem().toString());
		p.setFamilyMembersD(familyMembersD.getSelectedItem().toString());
		p.setPresentOccupation(presentOccupation.getSelectedItem().toString());
		p.setHowYouKnowIC(howYouKnowIC.getSelectedItem().toString());
		p.setFrequencyOfVisit(frequencyOfVisit.getSelectedItem().toString());
		p.setReasonForVisiting(reasonForVisiting.getSelectedItem().toString());
		p.setUploaded(uploaded);
		p.setProfileName();
		p.setLongitude(longitude);
		p.setLatitude(latitude);
		return p;

	}

	@SuppressLint("SimpleDateFormat")
	protected boolean validate() {

		// Reset errors.
		registrationDate.setError(null);
		
		List<EditText> etList = new ArrayList<EditText>();
		etList.add(fname);
		etList.add(lname);
		etList.add(phone);
		etList.add(permanentWard);
		if(!temporarySameAsPermanent.isChecked()){
			etList.add(temporaryWard);
		}
		
		Map<String,Spinner> spMap = new HashMap<String,Spinner>();
		spMap.put("District", district);
		spMap.put("Age",age);
		spMap.put("Ethnicity",casteEthnicity);
		spMap.put("Caste",casteCaste);
		
		spMap.put("Permanent District", permanentDistrict);
		spMap.put("Permanent VDC", permanentVDC);
		if(!temporarySameAsPermanent.isChecked()){
			spMap.put("Temporary District", temporaryDistrict);
			spMap.put("Temporary VDC", temporaryDistrict);
		}
		spMap.put("Educational Status", educationalStatus);
		spMap.put("Marital Status", maritalStatus);
		spMap.put("Present Occupation", presentOccupation);
		spMap.put("How do you know about IC?", howYouKnowIC);
		

		if (TextUtils.isEmpty(registrationDate.getText())) {
			registrationDate.setError("Please select registration date.");
			return false;
		} else {
			SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			try {
				df.parse(registrationDate.getText().toString());
			} catch (ParseException e) {
				registrationDate.setError("Please enter valid date");
				return false;
			}
		}

		for(EditText et:etList){
			if(!validateText(et)){
				return false;
			}
		}
		
		for(String sp:spMap.keySet()){
		if (!spinnerValidate(spMap.get(sp), "Please select "+sp)) {
			return false;
		}}
		
		if (!radiogroupValidate(gender, "Please choose Gender")){
			return false;
		}
		
		if (!radiogroupValidate(discrimination, "Please choose Discrimination")){
			return false;
		}

		return true;
	}

	private boolean validateText(EditText et) {
		et.setError(null);
		if (TextUtils.isEmpty(et.getText())) {
			et.setError(et.getHint() + " is Required.");
			et.requestFocus();
			return false;
		}
		return true;
	}

	private boolean spinnerValidate(Spinner sp, String msg) {
		int pos = sp.getSelectedItemPosition();
		if (pos == 0) {
			Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}
	
	private boolean radiogroupValidate(RadioGroup rg, String msg) {
		if (rg.getCheckedRadioButtonId() == -1) {
			Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}

	protected void addProfile() {
		// validation
		if (!validate()) {
			return;
		}
		//
		getProfileManagerApplication().addProfile(profiler());
		finish();
	}

	
	
	
	
		
	public void showDatePickerDialog(View v) {
		DialogFragment newFragment = new DatePickerFragment(
				(EditText) findViewById(R.id.registrationDate));
		newFragment.show(getFragmentManager(), "datePicker");
	}

	private void setUpViews(int position) {
		addButton = (Button) findViewById(R.id.add_button);
		cancelButton = (Button) findViewById(R.id.cancel_button);
		picture = (ImageView)findViewById(R.id.picture);
		if (position != -1) {

			addButton.setText(res.getString(R.string.save));
			cancelButton.setText(res.getString(R.string.delete));

			addButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					
					if(!uploaded){
						editProfile();
					}else{
						finish();
					}
				}
			});

			cancelButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					delete();
				}
			});
		} else {

			addButton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					addProfile();
				}
			});

			cancelButton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					cancel();
				}
			});
		}
		
		picture = (ImageView) findViewById(R.id.picture);
		informationSource = (Spinner) findViewById(R.id.informationSource);
		// profileNumber = (EditText) findViewById(R.id.profileNumber);
		registrationNumber = (EditText) findViewById(R.id.registrationNumber);
		registrationDate = (EditText) findViewById(R.id.registrationDate);
		district = (Spinner) findViewById(R.id.district);
		gender = (RadioGroup) findViewById(R.id.gender);
		fname = (EditText) findViewById(R.id.fname);
		mname = (EditText) findViewById(R.id.mname);
		lname = (EditText) findViewById(R.id.lname);
		age = (Spinner) findViewById(R.id.age);
		casteEthnicity = (Spinner) findViewById(R.id.ethnicity);
		casteCaste = (Spinner) findViewById(R.id.caste);
		discrimination = (RadioGroup) findViewById(R.id.discrimination);
		phone = (EditText) findViewById(R.id.phone);
		phoneFamily = (EditText) findViewById(R.id.phoneFamily);
		permanentDistrict = (Spinner) findViewById(R.id.permanentDistrict);
		permanentVDC = (Spinner) findViewById(R.id.permanentVDC);
		permanentWard = (EditText) findViewById(R.id.permanentWard);
		permanentDetails = (EditText) findViewById(R.id.permanentDetails);
		temporarySameAsPermanent = (CheckBox) findViewById(R.id.temporarySameAsPermanent);
		temporaryDistrict = (Spinner) findViewById(R.id.temporaryDistrict);
		temporaryVDC = (Spinner) findViewById(R.id.temporaryVDC);
		//.setH
		temporaryWard = (EditText) findViewById(R.id.temporaryWard);
		temporaryDetails = (EditText) findViewById(R.id.temporaryDetails);
		educationalStatus = (Spinner) findViewById(R.id.educationalStatus);
		maritalStatus = (Spinner) findViewById(R.id.maritalStatus);
		familyMembersA = (Spinner) findViewById(R.id.familyMemberA);
		familyMembersB = (Spinner) findViewById(R.id.familyMemberB);
		familyMembersC = (Spinner) findViewById(R.id.familyMemberC);
		familyMembersD = (Spinner) findViewById(R.id.familyMemberD);
		presentOccupation = (Spinner) findViewById(R.id.presentOccupation);
		howYouKnowIC = (Spinner) findViewById(R.id.howYouKnowIC);
		frequencyOfVisit = (Spinner) findViewById(R.id.frequencyOfVisit);
		reasonForVisiting = (Spinner) findViewById(R.id.reasonForVisiting);

		
		picture.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				changesPending=true;
				captureImage(v);
				
			}
			
		});;
		informationSource.setOnItemSelectedListener(itemSelectorListener());
		registrationNumber.addTextChangedListener(textChanged());
		registrationDate.addTextChangedListener(textChanged());
		district.setOnItemSelectedListener(itemSelectorListener());
		gender.setOnCheckedChangeListener(radioCheckChanged());
		fname.addTextChangedListener(textChanged());
		mname.addTextChangedListener(textChanged());
		lname.addTextChangedListener(textChanged());
		age.setOnItemSelectedListener(itemSelectorListener());
		
		
		final List<ArrayAdapter<CharSequence>> casteList = initCasteArrayAdapter();
		final List<ArrayAdapter<CharSequence>> vdcList = initVDCArrayAdapter();
		casteEthnicity.setOnItemSelectedListener(spinnerSelectedListener(
				casteCaste, casteList));
		casteCaste.setOnItemSelectedListener(itemSelectorListener());
		discrimination.setOnCheckedChangeListener(radioCheckChanged());
		phone.addTextChangedListener(textChanged());
		phoneFamily.addTextChangedListener(textChanged());
		
		permanentDistrict.setOnItemSelectedListener(spinnerSelectedListener(
				permanentVDC, vdcList));
		permanentVDC.setOnItemSelectedListener(itemSelectorListener());
		permanentWard.addTextChangedListener(textChanged());
		permanentDetails.addTextChangedListener(textChanged());
		
		temporaryDistrict.setOnItemSelectedListener(spinnerSelectedListener(
				temporaryVDC, vdcList));
		temporarySameAsPermanent
				.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {

					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						pendingTrigger();
						if (isChecked) {
							temporaryDistrict.setClickable(false);
							temporaryVDC.setClickable(false);
							
							
							setSpinnerFromPosition(temporaryDistrict,
									permanentDistrict.getSelectedItemPosition());
							temporaryVDC.setAdapter(permanentVDC.getAdapter());
							setSpinnerFromPosition(temporaryVDC,
									permanentVDC.getSelectedItemPosition());
							
							//todo like a hard code
							temporaryVDCSelection = permanentVDC.getSelectedItemPosition();
							
							
							temporaryDetails.setText(permanentDetails.getText());
							temporaryWard.setText(permanentWard.getText());

							temporaryWard.setEnabled(false);
							temporaryDetails.setEnabled(false);

						} else {
							temporaryDistrict.setClickable(true);
							temporaryVDC.setClickable(true);
							temporaryWard.setEnabled(true);
							temporaryDetails.setEnabled(true);
						}
						

					}

				});
		temporaryVDC.setOnItemSelectedListener(itemSelectorListener());
		temporaryWard.addTextChangedListener(textChanged());
		temporaryDetails.addTextChangedListener(textChanged());
		educationalStatus.setOnItemSelectedListener(itemSelectorListener());
		maritalStatus.setOnItemSelectedListener(itemSelectorListener());
		familyMembersA.setOnItemSelectedListener(itemSelectorListener());
		familyMembersB.setOnItemSelectedListener(itemSelectorListener());
		familyMembersC.setOnItemSelectedListener(itemSelectorListener());
		familyMembersD.setOnItemSelectedListener(itemSelectorListener());
		presentOccupation.setOnItemSelectedListener(itemSelectorListener());
		howYouKnowIC.setOnItemSelectedListener(itemSelectorListener());
		frequencyOfVisit.setOnItemSelectedListener(itemSelectorListener());
		reasonForVisiting.setOnItemSelectedListener(itemSelectorListener());
		
	    // create class object
	    gps = new GPSTracker(AddProfileActivity.this);

	    // check if GPS enabled     
	    if(gps.canGetLocation()){
	         
	        latitude = gps.getLatitude();
	        longitude = gps.getLongitude();
	         
	        // \n is for new line
	        Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();    
	    }else{
	        // can't get location
	        // GPS or Network is not enabled
	        // Ask user to enable GPS/network in settings
	        gps.showSettingsAlert();
	    }

	}

	private OnItemSelectedListener itemSelectorListener() {
		return new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if(casteCaste.getSelectedItemPosition()==0 && casteSelection !=0)
					casteCaste.setSelection(casteSelection);
				if(permanentVDC.getSelectedItemPosition()==0 && permanentVDCSelection !=0)
					permanentVDC.setSelection(permanentVDCSelection);
				if(temporaryVDC.getSelectedItemPosition()==0 && temporaryVDCSelection !=0)
					temporaryVDC.setSelection(temporaryVDCSelection);
				changesPending=true;
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		};
	}

	private void setRadioFromString(RadioGroup rg, String s) {
		for (int i = 0; i < rg.getChildCount(); i++) {
			if (((RadioButton) rg.getChildAt(i)).getText().equals(s)) {
				rg.check(rg.getChildAt(i).getId());
			}
		}
	}

	
	private void setSpinnerFromString(Spinner sp, String s) {
		      sp.setSelection(getSpinnerPosition(sp,s));
	}

	private void setSpinnerFromPosition(Spinner sp, int p) {
		sp.setSelection(p);
	}

	private List<ArrayAdapter<CharSequence>> initCasteArrayAdapter() {

		List<ArrayAdapter<CharSequence>> arraylist = new ArrayList<ArrayAdapter<CharSequence>>();
		int[] ids = { R.array.caste_0, R.array.caste_1, R.array.caste_2,
				R.array.caste_3, R.array.caste_4, R.array.caste_5,
				R.array.caste_6, R.array.caste_7, R.array.caste_8,
				R.array.caste_9, R.array.caste_10, R.array.caste_11,
				R.array.caste_12 };
		for (int id : ids) {

			ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(this,
					android.R.layout.simple_spinner_item, res.getStringArray(id));
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			arraylist.add(adapter);

		}
		return arraylist;
	}

	private List<ArrayAdapter<CharSequence>> initVDCArrayAdapter() {

		List<ArrayAdapter<CharSequence>> arraylist = new ArrayList<ArrayAdapter<CharSequence>>();
		int[] ids = { R.array.vdc_0, R.array.vdc_4, R.array.vdc_6,
				R.array.vdc_13, R.array.vdc_14, R.array.vdc_15, R.array.vdc_16,
				R.array.vdc_17, R.array.vdc_18, R.array.vdc_19, R.array.vdc_20,
				R.array.vdc_21, R.array.vdc_23, R.array.vdc_27, R.array.vdc_28,
				R.array.vdc_30, R.array.vdc_40, R.array.vdc_48, R.array.vdc_56,
				R.array.vdc_71 };
		for (int id : ids) {
			ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(this,
					android.R.layout.simple_spinner_item, res.getStringArray(id));
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			arraylist.add(adapter);

		}
		return arraylist;
	}

	private OnItemSelectedListener spinnerSelectedListener(final Spinner sp,
			final List<ArrayAdapter<CharSequence>> arrayadapter) {

		return new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,
					View selectedItemView, int position, long id) {
					pendingTrigger();
				sp.setAdapter(arrayadapter.get(position));
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) {
				//setSpinnerFromPosition(sp, sp.getSelectedItemPosition());
			}

		};
	}

	private OnCheckedChangeListener radioCheckChanged() {
		return new OnCheckedChangeListener() {
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// int pos=gender.indexOfChild(findViewById(checkedId));

				// Toast.makeText(getBaseContext(),
				// "Method 1 ID = "+String.valueOf(pos),
				// Toast.LENGTH_SHORT).show();
pendingTrigger();
				if (group.getId() == R.id.gender && checkedId == R.id.female) {
					((RadioGroup) findViewById(R.id.discrimination))
							.check(R.id.discriminated);

				}
			}
		};
	}
	
	private void pendingTrigger() {
		changesPending = true;
		
		//addButton.setText(res.getString(R.string.save));
	}

	private TextWatcher textChanged() {
		return new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				pendingTrigger();
			}
		

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		};
	}
	
	/**
	   * Handle user returning from both capturing and cropping the image
	   */
	  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	  	if (resultCode == RESULT_OK) {
	  		//user is returning from capturing an image using the camera
	  		if(requestCode == CAMERA_CAPTURE){
	  			//get the Uri for the captured image
	  			
	  			//picUri = data.getData();
	  			//Log.d("ShootAndCaptureActivity", picUri.toString());
	  			
	  			//carry out the crop operation
	  			finalPic = picUri;
	  			performCrop();
	  		}
	  		//user is returning from cropping the image
	  		else if(requestCode == PIC_CROP && null != data ){
	  			//get the returned data
	  			Bundle extras = data.getExtras();
	  			//get the cropped bitmap
	  			Bitmap thePic = extras.getParcelable("data");
	  			
	  			//display the returned cropped image
	  			picture.setVisibility(View.VISIBLE);
	  			picture.setImageBitmap(thePic);
	  			
	  			/*File f = new File(picUri.getPath());            
	            
	  			//String path = Environment.getExternalStorageDirectory().toString();
	  			OutputStream fOut = null;
	  			File file = new File(picUri.getPath());
	  			try{
	  				if (f.exists()) {
	  	                f.delete();
	  	            }
	  			fOut = new FileOutputStream(file);

	  			thePic.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
	  			fOut.flush();
	  			fOut.close();

	  			MediaStore.Images.Media.insertImage(getContentResolver(),file.getAbsolutePath(),file.getName(),file.getName());
	  			
	  			}catch(Exception ex){
	  				
	  			}
	*/  		}else if (requestCode == PIC_GALLERY && null != data) {
        Uri selectedImage = data.getData();
        String[] filePathColumn = { MediaStore.Images.Media.DATA };

        Cursor cursor = getContentResolver().query(selectedImage,
                filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        
        picUri = Uri.fromFile(new File(picturePath));
        cursor.close();
                     performCrop();
        
    }
	  	}
	  }
	  
	  /**
	   * Helper method to carry out crop operation
	   */
	  private void performCrop(){
	  	//take care of exceptions
	  	try {
	  		//call the standard crop action intent (the user device may not support it)
		    	Intent cropIntent = new Intent("com.android.camera.action.CROP"); 
		    	//indicate image type and Uri
		    	cropIntent.setDataAndType(picUri, "image/*");
		    	//set crop properties
		    	cropIntent.putExtra("crop", "true");
		    	//indicate aspect of desired crop
		    	cropIntent.putExtra("aspectX", 1);
		    	cropIntent.putExtra("aspectY", 1);
		    	//indicate output X and Y
		    	cropIntent.putExtra("outputX", 200);
		    	cropIntent.putExtra("outputY", 200);
		    	//retrieve data on return
		    	cropIntent.putExtra("return-data", true);
		    	cropIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, finalPic);
		    	//start the activity - we handle returning in onActivityResult
		        startActivityForResult(cropIntent, PIC_CROP);  
	  	}
	  	//respond to users whose devices do not support the crop action
	  	catch(ActivityNotFoundException anfe){
	  		//display an error message
	  		String errorMessage = "Whoops - your device doesn't support the crop action!";
	  		Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
	  		toast.show();
	  	}
	  }
	  
	  /**
	   * ------------ Helper Methods ---------------------- 
	   * */

	  /**
	   * Creating file uri to store image/video
	   */
	  public Uri getOutputMediaFileUri(int type) {
	      return Uri.fromFile(getOutputMediaFile(type));
	  }

	  /**
	   * returning image / video
	   */
	  private static File getOutputMediaFile(int type) {

	      // External sdcard location
	      File mediaStorageDir = new File(
	              Environment
	                      .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
	              IMAGE_DIRECTORY_NAME);

	      // Create the storage directory if it does not exist
	      if (!mediaStorageDir.exists()) {
	          if (!mediaStorageDir.mkdirs()) {
	              Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
	                      + IMAGE_DIRECTORY_NAME + " directory");
	              return null;
	          }
	      }

	      // Create a media file name
	      String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
	              Locale.getDefault()).format(new Date());
	      File mediaFile;
	      if (type == MEDIA_TYPE_IMAGE) {
	          mediaFile = new File(mediaStorageDir.getPath() + File.separator
	                  + "IMG_" + timeStamp + ".jpg");
	      } else {
	          return null;
	      }

	      return mediaFile;
	  }
	  
	
  

  /**
   * Checking device has camera hardware or not
   * */
  private boolean isDeviceSupportCamera() {
      if (getApplicationContext().getPackageManager().hasSystemFeature(
              PackageManager.FEATURE_CAMERA)) {
      	            // this device has a camera
          return true;
      } else {
          // no camera on this device
          return false;
      }
  }

  /**
   * Capturing Camera Image will lauch camera app requrest image capture
   */
  public void captureImage(View v) {
	// Checking camera availability
      if (!isDeviceSupportCamera()) {
          Toast.makeText(getApplicationContext(),
                  "Sorry! Your device doesn't support camera",
                  Toast.LENGTH_LONG).show();
          // call for load intent.
        //use gallery to capture an image
          Intent pickIntent = new Intent(
        		  Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        		   
        		  startActivityForResult(pickIntent, PIC_GALLERY);
        	
            
        	 
            
        	
          
      }else{
    	//use standard intent to capture an image
      	Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
      	picUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
          
      	 
          captureIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, picUri);
      	//captureIntent.putExtra("return-data", true);
      	//we will handle the returned data in onActivityResult
          startActivityForResult(captureIntent, CAMERA_CAPTURE);
      }
  }

}
