package com.tagconsulting.samidb.views;

import com.tagconsulting.samidb.R;
import com.tagconsulting.samidb.profile.SaMProfile;
import com.tagconsulting.samidb.utilities.ImageUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ProfileListItem extends LinearLayout {

	private SaMProfile profile;
	private TextView checkbox;
	private ImageView image;
	private ImageView icon;
	
	private static int width;
	private static int height;
	
	public ProfileListItem(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	protected void onFinishInflate(){
		super.onFinishInflate();
		image = (ImageView) findViewById(android.R.id.icon);
		icon = (ImageView) findViewById(android.R.id.icon1);
		checkbox = (TextView) findViewById(android.R.id.text1);
		width = image.getDrawable().getIntrinsicWidth();
		height = image.getDrawable().getIntrinsicHeight();
	}
	
	public SaMProfile getProfile() {
		return profile;
	}

	public void setProfile(SaMProfile profile) {
		this.profile = profile;
		if(profile.getPicture()!=null){
			image.setImageBitmap(ImageUtils.getImageOfSize(profile.getPicture(),width,height));
		}
		else if(profile.getGender().equals("Male")){
			image.setImageBitmap(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.man_placeholder),width,height,true));
		}else if(profile.getGender().equals("Female")){
			image.setImageBitmap(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.woman_placeholder),width,height,true));
		}
		if(profile.isUploaded()){
			icon.setImageResource(android.R.drawable.presence_online);
		}else{
			icon.setImageResource(android.R.drawable.presence_invisible);
		}
		checkbox.setText(profile.getText());
		//checkbox.setChecked(profile.isComplete());
	}
	
}
