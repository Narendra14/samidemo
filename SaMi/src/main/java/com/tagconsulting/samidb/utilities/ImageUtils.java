package com.tagconsulting.samidb.utilities;

import java.io.ByteArrayOutputStream;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;

public class ImageUtils {
	public static byte[] getBytes(Bitmap bitmap) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(CompressFormat.JPEG, 100, stream);
		return stream.toByteArray();
	}

	public static Bitmap getImage(byte[] image) {
		return BitmapFactory.decodeByteArray(image, 0, image.length);
	}
	
	public static Bitmap getImageOfSize(byte[] image, int width, int height) {
		Bitmap bp = BitmapFactory.decodeByteArray(image, 0, image.length);
		bp = Bitmap.createScaledBitmap(bp, width, height, true);
		return bp;
	}

}