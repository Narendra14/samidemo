package com.tagconsulting.samidb.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

	public static final int VERSION = 1;
	public static final String DB_NAME = "sami_db.sqlite";
	public static final String PROFILE_TABLE = "profiles";
	public static final String PROFILE_ID = "id";
	public static final String INFORMATION_SOURCE = "informationSource";
	public static final String REGISTRATION_DATE = "registrationDate";
	public static final String DISTRICT = "district";
	public static final String GENDER = "gender";
	public static final String FIRST_NAME = "fname";
	public static final String MIDDLE_NAME = "mname";
	public static final String LAST_NAME = "lname";
	public static final String AGE = "age";
	public static final String ETHNICITY = "casteEthnicity";
	public static final String CASTE = "casteCaste";
	public static final String DISCRIMINATION = "isDiscriminated";
	public static final String PHONE = "phone";
	public static final String PHONE_FAMILY = "phoneFamily";
	public static final String PERMANENT_DISTRICT = "permanentDistrict";
	public static final String PERMANENT_VDC = "permanentVDC";
	public static final String PERMANET_WARD = "permanentWard";
	public static final String PERMANENT_DETAILS = "permanentDetails";
	public static final String TEMPORARY_SAME_AS_PERMAENT = "temporarySameAsPermanent";
	public static final String TEMPORARY_DISTRICT = "temporaryDistrict";
	public static final String TEMPORARY_VDC = "temporaryVDC";
	public static final String TEMPORARY_WARD = "temporaryWard";
	public static final String TEMPORARY_DETAILS = "temporaryDetails";
	public static final String EDUCATIONAL_STATUS = "educationalStatus";
	public static final String MARITAL_STATUS = "maritalStatus";
	public static final String FAMILY_MEMBERA = "familyMembersA";
	public static final String FAMILY_MEMBERB = "familyMembersB";
	public static final String FAMILY_MEMBERC = "familyMembersC";
	public static final String FAMILY_MEMBERD = "familyMembersD";
	public static final String PRESENT_OCCUPATION = "presentOccupation";
	public static final String PROFILE_UPLOADED = "uploaded";
	public static final String PICTURE = "picture";
	public static final String PROFILE_NAME = "fullName";
	public static final String REGISTRATION_NUMBER = "registrationNumber";
	public static final String HOW_YOU_KNOW_IC = "howYouKnowIC";
	public static final String FREQUENCY_OF_VISIT = "frequencyOfVisit";
	public static final String REASON_FOR_VISITING = "reasonForVisiting";
	public static final String GPS_LONGITUDE = "gpsLongitude";
	public static final String GPS_LATITUDE = "gpsLatitude";
	public static final String USER_ID = "userID";
	public static final String USER_TABLE = "userTable";
	public static final String USER_NAME = "userName";
	public static final String BASE_64 = "passWord";
	

	public DBHelper(Context context) {
		super(context, DB_NAME, null, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		dropAndCreate(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		/*if(oldVersion==1 && newVersion ==2){
			db.execSQL("ALTER TABLE " + PROFILE_TABLE + " ADD COLUMN " + PICTURE +";");
			db.execSQL("ALTER TABLE " + PROFILE_TABLE + " RENAME TO "+ PROFILE_TABLE+"_OLD"+";");
			

			createTables(db);
			db.execSQL("INSERT INTO " + PROFILE_TABLE + " ("+ PROFILE_ID +","+
					INFORMATION_SOURCE_NUMBER +","+
					REGISTRATION_DATE +","+
					DISTRICT +","+
					GENDER +","+
					FIRST_NAME +","+
					MIDDLE_NAME +","+
					LAST_NAME +","+
					AGE +","+
					ETHNICITY +","+
					CASTE +","+
					DISCRIMINATION +","+
					PHONE +","+
					PHONE_FAMILY +","+
					PERMANENT_DISTRICT +","+
					PERMANENT_VDC +","+
					PERMANET_WARD +","+
					PERMANENT_DETAILS +","+
					TEMPORARY_SAME_AS_PERMAENT +","+
					TEMPORARY_DISTRICT +","+
					TEMPORARY_VDC +","+
					TEMPORARY_WARD +","+
					TEMPORARY_DETAILS +","+
					EDUCATIONAL_STATUS +","+
					MARITAL_STATUS +","+
					FAMILY_MEMBERA +","+
					FAMILY_MEMBERB +","+
					FAMILY_MEMBERC +","+
					FAMILY_MEMBERD +","+
					PRESENT_OCCUPATION +","+
					PROFILE_UPLOADED +")" + " SELECT "+ PROFILE_ID +","+
					INFORMATION_SOURCE_NUMBER +","+
					REGISTRATION_DATE +","+
					DISTRICT +","+
					GENDER +","+
					FIRST_NAME +","+
					MIDDLE_NAME +","+
					LAST_NAME +","+
					AGE +","+
					ETHNICITY +","+
					CASTE +","+
					DISCRIMINATION +","+
					PHONE +","+
					PHONE_FAMILY +","+
					PERMANENT_DISTRICT +","+
					PERMANENT_VDC +","+
					PERMANET_WARD +","+
					PERMANENT_DETAILS +","+
					TEMPORARY_SAME_AS_PERMAENT +","+
					TEMPORARY_DISTRICT +","+
					TEMPORARY_VDC +","+
					TEMPORARY_WARD +","+
					TEMPORARY_DETAILS +","+
					EDUCATIONAL_STATUS +","+
					MARITAL_STATUS +","+
					FAMILY_MEMBERA +","+
					FAMILY_MEMBERB +","+
					FAMILY_MEMBERC +","+
					FAMILY_MEMBERD +","+
					PRESENT_OCCUPATION +","+
					PROFILE_UPLOADED + " FROM " + PROFILE_TABLE+"_OLD"+";");
			db.execSQL("DROP TABLE " + PROFILE_TABLE+"_OLD"+";");
		}
		if(oldVersion==2 && newVersion ==3){
			db.execSQL("ALTER TABLE " + PROFILE_TABLE + " ADD COLUMN " + PROFILE_NAME +";");
			db.execSQL("ALTER TABLE " + PROFILE_TABLE + " RENAME TO "+ PROFILE_TABLE+"_OLD"+";");
			

			createTables(db);
			db.execSQL("INSERT INTO " + PROFILE_TABLE + " ("+ PROFILE_ID +","+
					INFORMATION_SOURCE_NUMBER +","+
					REGISTRATION_DATE +","+
					DISTRICT +","+
					GENDER +","+
					FIRST_NAME +","+
					MIDDLE_NAME +","+
					LAST_NAME +","+
					AGE +","+
					ETHNICITY +","+
					CASTE +","+
					DISCRIMINATION +","+
					PHONE +","+
					PHONE_FAMILY +","+
					PERMANENT_DISTRICT +","+
					PERMANENT_VDC +","+
					PERMANET_WARD +","+
					PERMANENT_DETAILS +","+
					TEMPORARY_SAME_AS_PERMAENT +","+
					TEMPORARY_DISTRICT +","+
					TEMPORARY_VDC +","+
					TEMPORARY_WARD +","+
					TEMPORARY_DETAILS +","+
					EDUCATIONAL_STATUS +","+
					MARITAL_STATUS +","+
					FAMILY_MEMBERA +","+
					FAMILY_MEMBERB +","+
					FAMILY_MEMBERC +","+
					FAMILY_MEMBERD +","+
					PRESENT_OCCUPATION +","+
					PROFILE_UPLOADED +","+
					PICTURE + ")" + " SELECT "+ PROFILE_ID +","+
					INFORMATION_SOURCE_NUMBER +","+
					REGISTRATION_DATE +","+
					DISTRICT +","+
					GENDER +","+
					FIRST_NAME +","+
					MIDDLE_NAME +","+
					LAST_NAME +","+
					AGE +","+
					ETHNICITY +","+
					CASTE +","+
					DISCRIMINATION +","+
					PHONE +","+
					PHONE_FAMILY +","+
					PERMANENT_DISTRICT +","+
					PERMANENT_VDC +","+
					PERMANET_WARD +","+
					PERMANENT_DETAILS +","+
					TEMPORARY_SAME_AS_PERMAENT +","+
					TEMPORARY_DISTRICT +","+
					TEMPORARY_VDC +","+
					TEMPORARY_WARD +","+
					TEMPORARY_DETAILS +","+
					EDUCATIONAL_STATUS +","+
					MARITAL_STATUS +","+
					FAMILY_MEMBERA +","+
					FAMILY_MEMBERB +","+
					FAMILY_MEMBERC +","+
					FAMILY_MEMBERD +","+
					PRESENT_OCCUPATION +","+
					PROFILE_UPLOADED + ","+
					PICTURE + " FROM " + PROFILE_TABLE+"_OLD"+";");
			db.execSQL("DROP TABLE " + PROFILE_TABLE+"_OLD"+";");
		}
			
		if(oldVersion==2 && newVersion ==4){
			db.execSQL("ALTER TABLE " + PROFILE_TABLE + " ADD COLUMN " + PROFILE_NAME +";");
			db.execSQL("ALTER TABLE " + PROFILE_TABLE + " ADD COLUMN " + GPS_LONGITUDE +";");
			db.execSQL("ALTER TABLE " + PROFILE_TABLE + " ADD COLUMN " + GPS_LATITUDE +";");
			db.execSQL("ALTER TABLE " + PROFILE_TABLE + " RENAME TO "+ PROFILE_TABLE+"_OLD"+";");
			

			createTables(db);
			db.execSQL("INSERT INTO " + PROFILE_TABLE + " ("+ PROFILE_ID +","+
					INFORMATION_SOURCE_NUMBER +","+
					REGISTRATION_DATE +","+
					DISTRICT +","+
					GENDER +","+
					FIRST_NAME +","+
					MIDDLE_NAME +","+
					LAST_NAME +","+
					AGE +","+
					ETHNICITY +","+
					CASTE +","+
					DISCRIMINATION +","+
					PHONE +","+
					PHONE_FAMILY +","+
					PERMANENT_DISTRICT +","+
					PERMANENT_VDC +","+
					PERMANET_WARD +","+
					PERMANENT_DETAILS +","+
					TEMPORARY_SAME_AS_PERMAENT +","+
					TEMPORARY_DISTRICT +","+
					TEMPORARY_VDC +","+
					TEMPORARY_WARD +","+
					TEMPORARY_DETAILS +","+
					EDUCATIONAL_STATUS +","+
					MARITAL_STATUS +","+
					FAMILY_MEMBERA +","+
					FAMILY_MEMBERB +","+
					FAMILY_MEMBERC +","+
					FAMILY_MEMBERD +","+
					PRESENT_OCCUPATION +","+
					PROFILE_UPLOADED +","+
					PICTURE + ")" + " SELECT "+ PROFILE_ID +","+
					INFORMATION_SOURCE_NUMBER +","+
					REGISTRATION_DATE +","+
					DISTRICT +","+
					GENDER +","+
					FIRST_NAME +","+
					MIDDLE_NAME +","+
					LAST_NAME +","+
					AGE +","+
					ETHNICITY +","+
					CASTE +","+
					DISCRIMINATION +","+
					PHONE +","+
					PHONE_FAMILY +","+
					PERMANENT_DISTRICT +","+
					PERMANENT_VDC +","+
					PERMANET_WARD +","+
					PERMANENT_DETAILS +","+
					TEMPORARY_SAME_AS_PERMAENT +","+
					TEMPORARY_DISTRICT +","+
					TEMPORARY_VDC +","+
					TEMPORARY_WARD +","+
					TEMPORARY_DETAILS +","+
					EDUCATIONAL_STATUS +","+
					MARITAL_STATUS +","+
					FAMILY_MEMBERA +","+
					FAMILY_MEMBERB +","+
					FAMILY_MEMBERC +","+
					FAMILY_MEMBERD +","+
					PRESENT_OCCUPATION +","+
					PROFILE_UPLOADED + ","+
					PICTURE + " FROM " + PROFILE_TABLE+"_OLD"+";");
		
		db.execSQL("DROP TABLE " + PROFILE_TABLE+"_OLD"+";");
	}
		
			if(oldVersion==3 && newVersion ==4){
				db.execSQL("ALTER TABLE " + PROFILE_TABLE + " ADD COLUMN " + GPS_LONGITUDE +";");
				db.execSQL("ALTER TABLE " + PROFILE_TABLE + " ADD COLUMN " + GPS_LATITUDE +";");
				db.execSQL("ALTER TABLE " + PROFILE_TABLE + " RENAME TO "+ PROFILE_TABLE+"_OLD"+";");
				

				createTables(db);
				db.execSQL("INSERT INTO " + PROFILE_TABLE + " ("+ PROFILE_ID +","+
						INFORMATION_SOURCE_NUMBER +","+
						REGISTRATION_DATE +","+
						DISTRICT +","+
						GENDER +","+
						FIRST_NAME +","+
						MIDDLE_NAME +","+
						LAST_NAME +","+
						AGE +","+
						ETHNICITY +","+
						CASTE +","+
						DISCRIMINATION +","+
						PHONE +","+
						PHONE_FAMILY +","+
						PERMANENT_DISTRICT +","+
						PERMANENT_VDC +","+
						PERMANET_WARD +","+
						PERMANENT_DETAILS +","+
						TEMPORARY_SAME_AS_PERMAENT +","+
						TEMPORARY_DISTRICT +","+
						TEMPORARY_VDC +","+
						TEMPORARY_WARD +","+
						TEMPORARY_DETAILS +","+
						EDUCATIONAL_STATUS +","+
						MARITAL_STATUS +","+
						FAMILY_MEMBERA +","+
						FAMILY_MEMBERB +","+
						FAMILY_MEMBERC +","+
						FAMILY_MEMBERD +","+
						PRESENT_OCCUPATION +","+
						PROFILE_UPLOADED +","+
						PICTURE + ","+
						PROFILE_NAME+ ")" + " SELECT "+ PROFILE_ID +","+
						INFORMATION_SOURCE_NUMBER +","+
						REGISTRATION_DATE +","+
						DISTRICT +","+
						GENDER +","+
						FIRST_NAME +","+
						MIDDLE_NAME +","+
						LAST_NAME +","+
						AGE +","+
						ETHNICITY +","+
						CASTE +","+
						DISCRIMINATION +","+
						PHONE +","+
						PHONE_FAMILY +","+
						PERMANENT_DISTRICT +","+
						PERMANENT_VDC +","+
						PERMANET_WARD +","+
						PERMANENT_DETAILS +","+
						TEMPORARY_SAME_AS_PERMAENT +","+
						TEMPORARY_DISTRICT +","+
						TEMPORARY_VDC +","+
						TEMPORARY_WARD +","+
						TEMPORARY_DETAILS +","+
						EDUCATIONAL_STATUS +","+
						MARITAL_STATUS +","+
						FAMILY_MEMBERA +","+
						FAMILY_MEMBERB +","+
						FAMILY_MEMBERC +","+
						FAMILY_MEMBERD +","+
						PRESENT_OCCUPATION +","+
						PROFILE_UPLOADED + ","+
						PICTURE + ","+
						PROFILE_NAME + " FROM " + PROFILE_TABLE+"_OLD"+";");
			
			db.execSQL("DROP TABLE " + PROFILE_TABLE+"_OLD"+";");
		}*/
	}

	protected void dropAndCreate(SQLiteDatabase db) {
		db.execSQL("drop table if exists " + PROFILE_TABLE + ";");
		createTables(db);
	}

	protected void createTables(SQLiteDatabase db) {
		db.execSQL("create table " + PROFILE_TABLE + " (" + 
				PROFILE_ID 	+ " integer primary key autoincrement not null," + 
				INFORMATION_SOURCE + " text," + 
				//PROFILE_NUMBER + " text," + 
				REGISTRATION_DATE + " text," + 
				DISTRICT + " text," +
				GENDER + " text," +
				FIRST_NAME + " text," + 
				MIDDLE_NAME + " text," + 
				LAST_NAME + " text," + 
				AGE + " text," + 
				ETHNICITY + " text," + 
				CASTE + " text," + 
				DISCRIMINATION + " text," + 
				PHONE + " text," + 
				PHONE_FAMILY + " text," + 
				PERMANENT_DISTRICT + " text," + 
				PERMANENT_VDC + " text," + 
				PERMANET_WARD + " text," +
				PERMANENT_DETAILS + " text," + 
				TEMPORARY_SAME_AS_PERMAENT + " text," + 
				TEMPORARY_DISTRICT + " text," + 
				TEMPORARY_VDC + " text," + 
				TEMPORARY_WARD + " text," + 
				TEMPORARY_DETAILS + " text," + 
				EDUCATIONAL_STATUS + " text," + 
				MARITAL_STATUS + " text," + 
				FAMILY_MEMBERA + " text," + 
				FAMILY_MEMBERB + " text," + 
				FAMILY_MEMBERC + " text," + 
				FAMILY_MEMBERD + " text," + 
				PRESENT_OCCUPATION + " text," + 
				//WAGE + " text null," + 
				PROFILE_UPLOADED + " text," +
				PICTURE + " text,"+
				PROFILE_NAME + " text,"+
				REGISTRATION_NUMBER + " text,"+
				HOW_YOU_KNOW_IC + " text,"+
				FREQUENCY_OF_VISIT + " text,"+
				REASON_FOR_VISITING + " text,"+
				GPS_LONGITUDE + " double,"+
				GPS_LATITUDE + " double"+");");
		/*db.execSQL("create table " + USER_TABLE + " (" +
				USER_ID + " integer primary key not null," + 
				USER_TABLE + " text," + 
				//PROFILE_NUMBER + " text," + 
				BASE_64 + " text"+");");*/
		
	}
	
	
}
