package com.tagconsulting.samidb.provider;

import android.content.SearchRecentSuggestionsProvider;

public class SuggestionProvider extends SearchRecentSuggestionsProvider {
    
    public final static int MODE = DATABASE_MODE_QUERIES;
	public final static String AUTHORITY = "com.ngoutsourcing.samidb.provider.SuggestionProvider";
    public SuggestionProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }
}
