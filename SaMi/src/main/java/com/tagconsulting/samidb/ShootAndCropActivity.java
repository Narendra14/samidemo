package com.tagconsulting.samidb;

//ADD PACKAGE HERE

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

/**
* ShootAndCropActivity demonstrates capturing and cropping camera images
* - user presses button to capture an image using the device camera
* - when they return with the captured image Uri, the app launches the crop action intent
* - on returning from the crop action, the app displays the cropped image
* 
* Sue Smith
* Mobiletuts+ Tutorial: Capturing and Cropping an Image with the Android Camera
* July 2012
*
*/
public class ShootAndCropActivity extends Activity implements OnClickListener {
	
	//keep track of camera capture intent
	final int CAMERA_CAPTURE = 1;
	//keep track of cropping intent
	final int PIC_CROP = 2;
	//captured picture uri
	private Uri picUri;
	public static final int MEDIA_TYPE_IMAGE = 1;
	
	// directory name to store captured images and videos
    private static final String IMAGE_DIRECTORY_NAME = "SaMiDB";
	
  /** Called when the activity is first created. */
  @Override
  public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.shoot_capture);
      
      //retrieve a reference to the UI button
      Button captureBtn = (Button)findViewById(R.id.capture_btn);
      //handle button clicks
      captureBtn.setOnClickListener(this);
  }

  /**
   * Click method to handle user pressing button to launch camera
   */
  public void onClick(View v) {
      if (v.getId() == R.id.capture_btn) {     
      	try {
	        	//use standard intent to capture an image
	        	Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	        	picUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
	            
	        	 
	            captureIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, picUri);
	        	//captureIntent.putExtra("return-data", true);
	        	//we will handle the returned data in onActivityResult
	            startActivityForResult(captureIntent, CAMERA_CAPTURE);
      	}
          catch(ActivityNotFoundException anfe){
      		//display an error message
      		String errorMessage = "Whoops - your device doesn't support capturing images!";
      		Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
      		toast.show();
      	}
      }
  }
  
  /**
   * Handle user returning from both capturing and cropping the image
   */
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
  	if (resultCode == RESULT_OK) {
  		//user is returning from capturing an image using the camera
  		if(requestCode == CAMERA_CAPTURE){
  			//get the Uri for the captured image
  			
  			//picUri = data.getData();
  			//Log.d("ShootAndCaptureActivity", picUri.toString());
  			
  			//carry out the crop operation
  			performCrop();
  		}
  		//user is returning from cropping the image
  		else if(requestCode == PIC_CROP){
  			//get the returned data
  			Bundle extras = data.getExtras();
  			//get the cropped bitmap
  			Bitmap thePic = extras.getParcelable("data");
  			//retrieve a reference to the ImageView
  			ImageView picView = (ImageView)findViewById(R.id.picture);
  			//display the returned cropped image
  			picView.setImageBitmap(thePic);
  			
  			/*File f = new File(picUri.getPath());            
            
  			//String path = Environment.getExternalStorageDirectory().toString();
  			OutputStream fOut = null;
  			File file = new File(picUri.getPath());
  			try{
  				if (f.exists()) {
  	                f.delete();
  	            }
  			fOut = new FileOutputStream(file);

  			thePic.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
  			fOut.flush();
  			fOut.close();

  			MediaStore.Images.Media.insertImage(getContentResolver(),file.getAbsolutePath(),file.getName(),file.getName());
  			
  			}catch(Exception ex){
  				
  			}
*/  		}
  	}
  }
  
  /**
   * Helper method to carry out crop operation
   */
  private void performCrop(){
  	//take care of exceptions
  	try {
  		//call the standard crop action intent (the user device may not support it)
	    	Intent cropIntent = new Intent("com.android.camera.action.CROP"); 
	    	//indicate image type and Uri
	    	cropIntent.setDataAndType(picUri, "image/*");
	    	//set crop properties
	    	cropIntent.putExtra("crop", "true");
	    	//indicate aspect of desired crop
	    	cropIntent.putExtra("aspectX", 1);
	    	cropIntent.putExtra("aspectY", 1);
	    	//indicate output X and Y
	    	cropIntent.putExtra("outputX", 200);
	    	cropIntent.putExtra("outputY", 200);
	    	//retrieve data on return
	    	cropIntent.putExtra("return-data", true);
	    	cropIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, picUri);
	    	//start the activity - we handle returning in onActivityResult
	        startActivityForResult(cropIntent, PIC_CROP);  
  	}
  	//respond to users whose devices do not support the crop action
  	catch(ActivityNotFoundException anfe){
  		//display an error message
  		String errorMessage = "Whoops - your device doesn't support the crop action!";
  		Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
  		toast.show();
  	}
  }
  
  /**
   * ------------ Helper Methods ---------------------- 
   * */

  /**
   * Creating file uri to store image/video
   */
  public Uri getOutputMediaFileUri(int type) {
      return Uri.fromFile(getOutputMediaFile(type));
  }

  /**
   * returning image / video
   */
  private static File getOutputMediaFile(int type) {

      // External sdcard location
      File mediaStorageDir = new File(
              Environment
                      .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
              IMAGE_DIRECTORY_NAME);

      // Create the storage directory if it does not exist
      if (!mediaStorageDir.exists()) {
          if (!mediaStorageDir.mkdirs()) {
              Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                      + IMAGE_DIRECTORY_NAME + " directory");
              return null;
          }
      }

      // Create a media file name
      String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
              Locale.getDefault()).format(new Date());
      File mediaFile;
      if (type == MEDIA_TYPE_IMAGE) {
          mediaFile = new File(mediaStorageDir.getPath() + File.separator
                  + "IMG_" + timeStamp + ".jpg");
      } else {
          return null;
      }

      return mediaFile;
  }
  
}
