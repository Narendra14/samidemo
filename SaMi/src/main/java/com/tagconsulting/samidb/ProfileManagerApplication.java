package com.tagconsulting.samidb;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.tagconsulting.samidb.db.DBHelper;
import com.tagconsulting.samidb.profile.SaMProfile;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;

import static com.tagconsulting.samidb.db.DBHelper.*;

public class ProfileManagerApplication extends Application {

	private ArrayList<SaMProfile> currentProfiles;
	private SQLiteDatabase database;
	static private String authorization;
	static private String userName;

	@Override
	public void onCreate() {
		super.onCreate();
		DBHelper helper = new DBHelper(this);
		database = helper.getWritableDatabase();
		if (null == currentProfiles) {
			loadProfile();
		}
	}
	
	public boolean isLoggedin(){
		if (!TextUtils.isEmpty(getAuthorizationCode())){
			return true;
		}
		return false;
	}
	
	public void clearAuthorization(){
		/*String where = String.format("%s in (%s)", USER_ID, "1");
		database.delete(USER_TABLE, where, null);
		ContentValues values = new ContentValues();
		values.put(USER_ID, "1");
		values.put(USER_NAME, "");
		values.put(BASE_64, "");
		database.insert(USER_TABLE, null, values);*/
		authorization=null;
		userName=null;
	}
	
	public void setAuthorizationCode(String userName,String authorization){
		/*ContentValues values = new ContentValues();
		values.put(USER_NAME, userName);
		values.put(BASE_64, authorization);
		database.update(USER_TABLE, values, USER_ID + " = ?", new String[] { "1" });*/
		this.userName = userName;
		this.authorization = authorization;
		
	}

	public void setAuthorizationCode(String userName){
		/*ContentValues values = new ContentValues();
		values.put(USER_NAME, userName);
		values.put(BASE_64, authorization);
		database.update(USER_TABLE, values, USER_ID + " = ?", new String[] { "1" });*/
		this.userName = userName;

	}
	
	public String getAuthorizationCode(){
		/*String searchQuery = "SELECT * FROM "+ USER_TABLE+ ";";
		Cursor cursor = database.rawQuery(searchQuery, null );
		if(cursor!=null && cursor.getCount()>0){
			cursor.close();
			return cursor.getString(1);
		}
		else{
			cursor.close();
			ContentValues values = new ContentValues();
			values.put(USER_ID, "1");
			values.put(USER_NAME, "");
			values.put(BASE_64, "");
			database.insert(USER_TABLE, null, values);
			return null;			
		}*/
		return authorization;
		
	}
	
	public JSONArray getJSONFromDB()	{
		 
		String searchQuery = "SELECT * FROM "+PROFILE_TABLE+" WHERE "+PROFILE_UPLOADED+" = 'false';";
		Cursor cursor = database.rawQuery(searchQuery, null );
		 
		JSONArray resultSet 	= new JSONArray(); 
		//JSONObject returnObj 	= new JSONObject();
		 
		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
		 
		       	    int totalColumn = cursor.getColumnCount();
		       	    JSONObject rowObject = new JSONObject();
		 
		       	    for( int i=0 ;  i< totalColumn ; i++ )
		       	    {
		       	    	if( cursor.getColumnName(i) != null ) 
		       	    	{
		 
		       	    		try 
		       	    		{
		 
			       	    		if( cursor.getString(i) != null/* || (cursor.getString(i).startsWith("[") && (cursor.getString(i).endsWith("]")))*/)
			           	    	{
			           	    		//Log.d("JSON Buildup", cursor.getString(i) );
			           	    		rowObject.put(cursor.getColumnName(i) ,  cursor.getString(i) );
			           	    	}
			           	    	else
			           	    	{
			           	    		rowObject.put( cursor.getColumnName(i) ,  "" ); 
			           	    	}
		       	    		}
		       	    		catch( Exception e )
		       	    		{
		       	    			Log.d("JSON Buildup", e.getMessage()  );
		       	    		}
		       	    	}
		 
		       	    }
		 
		       	    resultSet.put(rowObject);
		       	    cursor.moveToNext();
		        }
		 
				cursor.close(); 
				//Log.d("Final JSON", resultSet.toString() );
				return resultSet; 
		 
		}
		

	public void updateProfile(long id){
		SaMProfile sp;
		
		for (int i=0; i<currentProfiles.size();i++){
			sp = currentProfiles.get(i);
		long ids=sp.getId();
			if(ids==id){
				sp.setUploaded(true);
				currentProfiles.set(i, sp);
				
				ContentValues newValues = new ContentValues();
				newValues.put(PROFILE_UPLOADED, "true");

				
				database.update(PROFILE_TABLE, newValues, PROFILE_ID + " = ?", new String[] { String.valueOf(sp.getId()) });
				
				//notify
			}
		}
	}
	
	private void loadProfile() {
		currentProfiles = new ArrayList<SaMProfile>();
		Cursor tasksCursor = database.query(PROFILE_TABLE, new String[] {
				PROFILE_ID, INFORMATION_SOURCE,
				REGISTRATION_DATE,DISTRICT,
				GENDER,FIRST_NAME,MIDDLE_NAME,LAST_NAME,
				AGE,ETHNICITY,CASTE, DISCRIMINATION,PHONE,PHONE_FAMILY, 
				PERMANENT_DETAILS,PERMANET_WARD, PERMANENT_VDC, PERMANENT_DISTRICT, 
				TEMPORARY_SAME_AS_PERMAENT,TEMPORARY_DETAILS,TEMPORARY_WARD, TEMPORARY_VDC, TEMPORARY_DISTRICT,
				EDUCATIONAL_STATUS,MARITAL_STATUS,FAMILY_MEMBERA, FAMILY_MEMBERB,FAMILY_MEMBERC, FAMILY_MEMBERD,
				PRESENT_OCCUPATION,PROFILE_UPLOADED,PICTURE,PROFILE_NAME,REGISTRATION_NUMBER, HOW_YOU_KNOW_IC,
				FREQUENCY_OF_VISIT,REASON_FOR_VISITING ,GPS_LONGITUDE,GPS_LATITUDE}, null, null, null,
				null, String.format("%s,%s", PROFILE_UPLOADED, PROFILE_NAME));
		tasksCursor.moveToFirst();
		SaMProfile p;
		if (!tasksCursor.isAfterLast()) {
			do {
				int id = tasksCursor.getInt(0);
				
				String informationSource = tasksCursor.getString(1);
				String registrationDate = tasksCursor.getString(2);
				String district = tasksCursor.getString(3);
				String gender = tasksCursor.getString(4);
				String fname = tasksCursor.getString(5);
				String mname = tasksCursor.getString(6);
				String lname = tasksCursor.getString(7);
				String age = tasksCursor.getString(8);
				String ethnicity = tasksCursor.getString(9);
				String caste = tasksCursor.getString(10);
				boolean discrimination = Boolean.parseBoolean(tasksCursor.getString(11));

				String phone = tasksCursor.getString(12);
				String phoneFamily = tasksCursor.getString(13);
				
				String permanentDetails = tasksCursor.getString(14);
				String permanentWard = tasksCursor.getString(15);
				String permanentVDC = tasksCursor.getString(16);
				String permanentDistrict = tasksCursor.getString(17);
				boolean temporarySameAsPermanent = Boolean.parseBoolean(tasksCursor.getString(18));
				String temporaryDetails = tasksCursor.getString(19);
				String temporaryWard = tasksCursor.getString(20);
				String temporaryVDC = tasksCursor.getString(21);
				String temporaryDistrict = tasksCursor.getString(22);
				
				String educationalStatus = tasksCursor.getString(23);
				String maritalStatus = tasksCursor.getString(24);
				String familyMembersA = tasksCursor.getString(25);
				String familyMembersB = tasksCursor.getString(26);
				String familyMembersC = tasksCursor.getString(27);
				String familyMembersD = tasksCursor.getString(28);
				String presentOccupation = tasksCursor.getString(29);
				boolean isUploaded = Boolean.parseBoolean(tasksCursor.getString(30));
				byte[] picture = tasksCursor.getBlob(31);
				String fullName = tasksCursor.getString(32);
				String registrationNumber =tasksCursor.getString(33);
				String howYouKnowIC=tasksCursor.getString(34);
				String frequencyOfVisit=tasksCursor.getString(35);
				String reasonForVisiting=tasksCursor.getString(36);
				Double longitude=tasksCursor.getDouble(37);
				Double latitude=tasksCursor.getDouble(38);
				
				p = new SaMProfile();
				p.setId(id);
				p.setPicture(picture);
				p.setInformationSource(informationSource);
				p.setRegistrationDate(registrationDate);
				p.setDistrict(district);
				p.setGender(gender);
				p.setFname(fname);
				p.setMname(mname);
				p.setLname(lname);
				p.setAge(age);
				p.setCasteEthnicity(ethnicity);
				p.setCasteCaste(caste);
				p.setDiscriminated(discrimination);
				p.setPhone(phone);
				p.setPhoneFamily(phoneFamily);
				
				p.setPermanentDetails(permanentDetails);
				p.setPermanentWard(permanentWard);
				p.setPermanentVDC(permanentVDC);
				p.setPermanentDistrict(permanentDistrict);
				p.setTemporarySameAsPermanent(temporarySameAsPermanent);
				p.setTemporaryDetails(temporaryDetails);
				p.setTemporaryWard(temporaryWard);
				p.setTemporaryVDC(temporaryVDC);
				p.setTemporaryDistrict(temporaryDistrict);
				p.setEducationalStatus(educationalStatus);
				p.setMaritalStatus(maritalStatus);
				p.setFamilyMembersA(familyMembersA);
				p.setFamilyMembersB(familyMembersB);
				p.setFamilyMembersC(familyMembersC);
				p.setFamilyMembersD(familyMembersD);
				p.setPresentOccupation(presentOccupation);
				p.setUploaded(isUploaded);
				p.setProfileName(fullName);
				p.setRegistrationNumber(registrationNumber);
				p.setHowYouKnowIC(howYouKnowIC);
				p.setFrequencyOfVisit(frequencyOfVisit);
				p.setReasonForVisiting(reasonForVisiting);
				p.setLongitude(longitude);
				p.setLatitude(latitude);
				currentProfiles.add(p);
			} while (tasksCursor.moveToNext());
		}

		tasksCursor.close();

	}

	public void setCurrentProfile(ArrayList<SaMProfile> currentProfile) {
		this.currentProfiles = currentProfile;
	}

	public ArrayList<SaMProfile> getCurrentProfile() {
		return currentProfiles;
	}

	public void deleteSelectedProfile(SaMProfile p) {
		StringBuffer idList = new StringBuffer();
		//for (int i = 0; i < ids.length; i++) {
			idList.append(p.getId());
			//if (i < ids.length - 1) {
				//idList.append(",");
			//}
		//}
		String where = String.format("%s in (%s)", PROFILE_ID, idList);
		database.delete(PROFILE_TABLE, where, null);
		currentProfiles.remove(p);
		
	}
	
	public void addProfile(SaMProfile p) {
		assert (null != p);
		ContentValues values = new ContentValues();
		values.put(PICTURE, p.getPicture());
		values.put(INFORMATION_SOURCE, p.getInformationSource());
		//values.put(PROFILE_NUMBER, p.getProfileNumber());
		values.put(REGISTRATION_DATE, p.getRegistrationDate());
		values.put(DISTRICT, p.getDistrict());
		values.put(GENDER, p.getGender());
		values.put(FIRST_NAME, p.getFname());
		values.put(MIDDLE_NAME, p.getMname());
		values.put(LAST_NAME, p.getLname());
		values.put(AGE, p.getAge());
		values.put(ETHNICITY, p.getCasteEthnicity());
		values.put(CASTE, p.getCasteCaste());
		values.put(DISCRIMINATION, Boolean.toString(p.isDiscriminated()));
		values.put(PHONE, p.getPhone());
		values.put(PHONE_FAMILY, p.getPhoneFamily());
		values.put(PERMANENT_DISTRICT, p.getPermanentDistrict());
		values.put(PERMANENT_VDC, p.getPermanentVDC());
		values.put(PERMANET_WARD, p.getPermanentWard());
		values.put(PERMANENT_DETAILS, p.getPermanentDetails());
		values.put(TEMPORARY_SAME_AS_PERMAENT, Boolean.toString(p.isTemporarySameAsPermanent()));
		values.put(TEMPORARY_DISTRICT, p.getTemporaryDistrict());
		values.put(TEMPORARY_VDC, p.getTemporaryVDC());
		values.put(TEMPORARY_WARD, p.getTemporaryWard());
		values.put(TEMPORARY_DETAILS, p.getTemporaryDetails());
		values.put(EDUCATIONAL_STATUS, p.getEducationalStatus());
		values.put(MARITAL_STATUS, p.getMaritalStatus());
		values.put(FAMILY_MEMBERA, p.getFamilyMembersA());
		values.put(FAMILY_MEMBERB, p.getFamilyMembersB());
		values.put(FAMILY_MEMBERC, p.getFamilyMembersC());
		values.put(FAMILY_MEMBERD, p.getFamilyMembersD());
		values.put(PRESENT_OCCUPATION, p.getPresentOccupation());
		//values.put(WAGE, p.getWage());
		values.put(PROFILE_UPLOADED, Boolean.toString(p.isUploaded()));
		values.put(PROFILE_NAME, p.getProfileName());
		values.put(REGISTRATION_NUMBER,p.getRegistrationNumber());
		values.put(HOW_YOU_KNOW_IC,p.getHowYouKnowIC());
		values.put(FREQUENCY_OF_VISIT,p.getFrequencyOfVisit());
		values.put(REASON_FOR_VISITING,p.getReasonForVisiting());
		values.put(GPS_LONGITUDE, p.getLongitude());
		values.put(GPS_LATITUDE,p.getLatitude());
		

		p.setId(database.insert(PROFILE_TABLE, null, values));
		currentProfiles.add(p);
	}

	public void editProfile(SaMProfile p) {
		//deleteSelectedProfile(p);
		//addProfile(p);
		editSelectedProfile(p);
	}

	private void editSelectedProfile(SaMProfile p) {
		//saveProfile(p);
		for (int i = 0; i < currentProfiles.size(); i++) {
			if (currentProfiles.get(i).getId() == p.getId())
			{
				currentProfiles.set(i, p);
			break;
			}
		}
		ContentValues values = new ContentValues();
		values.put(PICTURE, p.getPicture());
		values.put(INFORMATION_SOURCE, p.getInformationSource());
		//values.put(PROFILE_NUMBER, p.getProfileNumber());
		values.put(REGISTRATION_DATE, p.getRegistrationDate());
		values.put(DISTRICT, p.getDistrict());
		values.put(GENDER, p.getGender());
		values.put(FIRST_NAME, p.getFname());
		values.put(MIDDLE_NAME, p.getMname());
		values.put(LAST_NAME, p.getLname());
		values.put(AGE, p.getAge());
		values.put(ETHNICITY, p.getCasteEthnicity());
		values.put(CASTE, p.getCasteCaste());
		values.put(DISCRIMINATION, Boolean.toString(p.isDiscriminated()));
		values.put(PHONE, p.getPhone());
		values.put(PHONE_FAMILY, p.getPhoneFamily());
		values.put(PERMANENT_DISTRICT, p.getPermanentDistrict());
		values.put(PERMANENT_VDC, p.getPermanentVDC());
		values.put(PERMANET_WARD, p.getPermanentWard());
		values.put(PERMANENT_DETAILS, p.getPermanentDetails());
		values.put(TEMPORARY_SAME_AS_PERMAENT, Boolean.toString(p.isTemporarySameAsPermanent()));
		values.put(TEMPORARY_DISTRICT, p.getTemporaryDistrict());
		values.put(TEMPORARY_VDC, p.getTemporaryVDC());
		values.put(TEMPORARY_WARD, p.getTemporaryWard());
		values.put(TEMPORARY_DETAILS, p.getTemporaryDetails());
		values.put(EDUCATIONAL_STATUS, p.getEducationalStatus());
		values.put(MARITAL_STATUS, p.getMaritalStatus());
		values.put(FAMILY_MEMBERA, p.getFamilyMembersA());
		values.put(FAMILY_MEMBERB, p.getFamilyMembersB());
		values.put(FAMILY_MEMBERC, p.getFamilyMembersC());
		values.put(FAMILY_MEMBERD, p.getFamilyMembersD());
		values.put(PRESENT_OCCUPATION, p.getPresentOccupation());
		//values.put(WAGE, p.getWage());
		values.put(PROFILE_UPLOADED, Boolean.toString(p.isUploaded()));
		values.put(PROFILE_NAME, p.getProfileName());
		values.put(REGISTRATION_NUMBER,p.getRegistrationNumber());
		values.put(HOW_YOU_KNOW_IC,p.getHowYouKnowIC());
		values.put(FREQUENCY_OF_VISIT,p.getFrequencyOfVisit());
		values.put(REASON_FOR_VISITING,p.getReasonForVisiting());
		database.update(PROFILE_TABLE, values, PROFILE_ID + " = ?", new String[] { String.valueOf(p.getId()) });
	}

	@SuppressLint("DefaultLocale")
	/*public void saveProfile(SaMProfile p) {
		assert (null != p);
		ContentValues values = new ContentValues();
		values.put(PICTURE, p.getPicture());
		values.put(INFORMATION_SOURCE_NUMBER, p.getInformationSourceNumber());
		//values.put(PROFILE_NUMBER, p.getProfileNumber());
		values.put(REGISTRATION_DATE, p.getRegistrationDate());
		values.put(DISTRICT, p.getDistrict());
		values.put(GENDER, p.getGender());
		values.put(FIRST_NAME, p.getFname());
		values.put(MIDDLE_NAME, p.getMname());
		values.put(LAST_NAME, p.getLname());
		values.put(AGE, p.getAge());
		values.put(ETHNICITY, p.getCasteEthnicity());
		values.put(CASTE, p.getCasteCaste());
		values.put(DISCRIMINATION, Boolean.toString(p.isDiscriminated()));
		values.put(PHONE, p.getPhone());
		values.put(PHONE_FAMILY, p.getPhoneFamily());
		values.put(PERMANENT_DISTRICT, p.getPermanentDistrict());
		values.put(PERMANENT_VDC, p.getPermanentVDC());
		values.put(PERMANET_WARD, p.getPermanentWard());
		values.put(PERMANENT_DETAILS, p.getPermanentDetails());
		values.put(TEMPORARY_SAME_AS_PERMAENT, Boolean.toString(p.isTemporarySameAsPermanent()));
		values.put(TEMPORARY_DISTRICT, p.getTemporaryDistrict());
		values.put(TEMPORARY_VDC, p.getTemporaryVDC());
		values.put(TEMPORARY_WARD, p.getTemporaryWard());
		values.put(TEMPORARY_DETAILS, p.getTemporaryDetails());
		values.put(EDUCATIONAL_STATUS, p.getEducationalStatus());
		values.put(MARITAL_STATUS, p.getMaritalStatus());
		values.put(FAMILY_MEMBERA, p.getFamilyMembersA());
		values.put(FAMILY_MEMBERB, p.getFamilyMembersB());
		values.put(FAMILY_MEMBERC, p.getFamilyMembersC());
		values.put(FAMILY_MEMBERD, p.getFamilyMembersD());
		values.put(PRESENT_OCCUPATION, p.getPresentOccupation());
		//values.put(WAGE, p.getWage());
		values.put(PROFILE_UPLOADED, Boolean.toString(p.isUploaded()));
		values.put(PROFILE_NAME, p.getProfileName());

		long id = p.getId();
		String where = String.format("%s = %d", PROFILE_ID, id);

		database.update(PROFILE_TABLE, values, where, null);
	}
*/
	public void deleteProfile(Long[] ids) {
		StringBuffer idList = new StringBuffer();
		for (int i = 0; i < ids.length; i++) {
			idList.append(ids[i]);
			if (i < ids.length - 1) {
				idList.append(",");
			}
		}
		String where = String.format("%s in (%s)", PROFILE_ID, idList);
		database.delete(PROFILE_TABLE, where, null);
	}

	public SaMProfile getProfile(int position) {
		return currentProfiles.get(position);
	}

}
