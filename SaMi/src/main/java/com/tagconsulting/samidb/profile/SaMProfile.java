package com.tagconsulting.samidb.profile;

import java.io.Serializable;

import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;

public class SaMProfile implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6494960921314218414L;
	
	private byte[] picture;
	private String informationSource;
	//private String profileNumber;
	private String registrationDate;
	private String district;
	private String gender;
	private Object profile;
	private String fname;
	private String mname;
	private String lname;
	private String age;
	private String casteEthnicity;
	private String casteCaste;
	private boolean isDiscriminated;
	private String phone;
	private String phoneFamily;
	private String permanentDistrict;
	private String permanentVDC;
	private String permanentWard;
	private String permanentDetails;
	private boolean temporarySameAsPermanent;
	private String temporaryDistrict;
	private String temporaryVDC;
	private String temporaryWard;
	private String temporaryDetails;
	private String educationalStatus;
	private String maritalStatus;
	private String familyMembersA;
	private String familyMembersB;
	private String familyMembersC;
	private String familyMembersD;
	private String presentOccupation;
	//private String wage;
	private boolean uploaded;
	private String fullName;
	private long id;
	private String registrationNumber;
	private String howYouKnowIC;
	private String frequencyOfVisit;
	private String reasonForVisiting;
	
	private double longitude;
	private double latitude;

	public SpannableStringBuilder getText() {

		if(null==fullName)setProfileName();
		String details = new StringBuilder()/*.append(permanentDetails)*/.append("Ward ")
				.append(permanentWard).append(", ").append(permanentVDC).append(", ")
				.append(permanentDistrict).toString();

		SpannableStringBuilder sb = new SpannableStringBuilder(fullName + "\n"
				+ details + "\n" + maritalStatus.toString());

		// Span to make text bold
		final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); 


		sb.setSpan(bss, 0, fullName.length(),
				Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make name Bold

		return sb;

	}

	public SaMProfile() {
		this.profile = new Object();
	}

	public SaMProfile(Object profile) {
		this.profile = profile;
	}

	public Object getProfile() {
		return profile;
	}

	public void setProfile(Object[] profile) {
		this.profile = profile;
	}

	public boolean isUploaded() {
		return uploaded;
	}

	public void setUploaded(boolean isUploaded) {
		this.uploaded = isUploaded;
	}

	/*
	 * public void toggleComplete() { complete = !complete; }
	 */

	public void setId(long id) {
		this.id = id;

	}

	public long getId() {
		return id;
	}

	public String getInformationSource() {
		return informationSource;
	}

	public void setInformationSource(String informationSource) {
		this.informationSource = informationSource;
	}

	public String getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getCasteEthnicity() {
		return casteEthnicity;
	}

	public void setCasteEthnicity(String casteEthnicity) {
		this.casteEthnicity = casteEthnicity;
	}

	public String getCasteCaste() {
		return casteCaste;
	}

	public void setCasteCaste(String casteCaste) {
		this.casteCaste = casteCaste;
	}

	public boolean isDiscriminated() {
		return isDiscriminated;
	}

	public void setDiscriminated(boolean isDiscriminated) {
		this.isDiscriminated = isDiscriminated;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhoneFamily() {
		return phoneFamily;
	}

	public void setPhoneFamily(String phoneFamily) {
		this.phoneFamily = phoneFamily;
	}

	public String getPermanentDistrict() {
		return permanentDistrict;
	}

	public void setPermanentDistrict(String permanentDistrict) {
		this.permanentDistrict = permanentDistrict;
	}

	public String getPermanentVDC() {
		return permanentVDC;
	}

	public void setPermanentVDC(String permanentVDC) {
		this.permanentVDC = permanentVDC;
	}

	public String getPermanentWard() {
		return permanentWard;
	}

	public void setPermanentWard(String permanentWard) {
		this.permanentWard = permanentWard;
	}

	public String getPermanentDetails() {
		return permanentDetails;
	}

	public void setPermanentDetails(String permanentDetails) {
		this.permanentDetails = permanentDetails;
	}

	public boolean isTemporarySameAsPermanent() {
		return temporarySameAsPermanent;
	}

	public void setTemporarySameAsPermanent(boolean temporarySameAsPermanent) {
		this.temporarySameAsPermanent = temporarySameAsPermanent;
	}

	public String getTemporaryDistrict() {
		return temporaryDistrict;
	}

	public void setTemporaryDistrict(String temporaryDistrict) {
		this.temporaryDistrict = temporaryDistrict;
	}

	public String getTemporaryVDC() {
		return temporaryVDC;
	}

	public void setTemporaryVDC(String temporaryVDC) {
		this.temporaryVDC = temporaryVDC;
	}

	public String getTemporaryWard() {
		return temporaryWard;
	}

	public void setTemporaryWard(String temporaryWard) {
		this.temporaryWard = temporaryWard;
	}

	public String getTemporaryDetails() {
		return temporaryDetails;
	}

	public void setTemporaryDetails(String temporaryDetails) {
		this.temporaryDetails = temporaryDetails;
	}

	public String getEducationalStatus() {
		return educationalStatus;
	}

	public void setEducationalStatus(String educationalStatus) {
		this.educationalStatus = educationalStatus;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getFamilyMembersA() {
		return familyMembersA;
	}

	public void setFamilyMembersA(String familyMembersA) {
		this.familyMembersA = familyMembersA;
	}

	public String getFamilyMembersB() {
		return familyMembersB;
	}

	public void setFamilyMembersB(String familyMembersB) {
		this.familyMembersB = familyMembersB;
	}

	public String getFamilyMembersC() {
		return familyMembersC;
	}

	public void setFamilyMembersC(String familyMembersC) {
		this.familyMembersC = familyMembersC;
	}

	public String getFamilyMembersD() {
		return familyMembersD;
	}

	public void setFamilyMembersD(String familyMembersD) {
		this.familyMembersD = familyMembersD;
	}

	public String getPresentOccupation() {
		return presentOccupation;
	}

	public void setPresentOccupation(String presentOccupation) {
		this.presentOccupation = presentOccupation;
	}

	public byte[] getPicture() {
		return picture;
	}

	public void setPicture(byte[] picture) {
		this.picture = picture;
	}

	public void setProfileName(){
		fullName = new StringBuilder().append(fname.trim())
		.append(TextUtils.isEmpty(mname) ? " " : " " + mname.trim() + " ")
		.append(lname.trim()).toString();
	}
	
	public void setProfileName(String fullName){
		this.fullName = fullName;
	}
	
	
	public String getProfileName(){
		return fullName;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public String getHowYouKnowIC() {
		return howYouKnowIC;
	}

	public void setHowYouKnowIC(String howYouKnowIC) {
		this.howYouKnowIC = howYouKnowIC;
	}

	public String getFrequencyOfVisit() {
		return frequencyOfVisit;
	}

	public void setFrequencyOfVisit(String frequencyOfVisit) {
		this.frequencyOfVisit = frequencyOfVisit;
	}

	public String getReasonForVisiting() {
		return reasonForVisiting;
	}

	public void setReasonForVisiting(String reasonForVisiting) {
		this.reasonForVisiting = reasonForVisiting;
	}
}
